package com.pdd.article.tourist.api.controller;

import com.pdd.article.tourist.api.feign.API_Blog_Article;
import com.pdd.article.tourist.enums.WebResultEmun;
import com.pdd.article.tourist.response.ResponseResult;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author:liyangpeng
 * @date:2019/6/19 11:35
 */
@RestController
@RequestMapping("/blog_article")
public class Blog_ArticleController  {

    @Autowired
    private API_Blog_Article api_blog_article;

    /**
     * 查询列表文章接口
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @GetMapping("/getArticle")
    public ResponseEntity getArticle(Integer pageIndex, Integer pageSize){
        return api_blog_article.getArticle(pageIndex,pageSize);
    }

    /**
     * 查询用户主页列表文章
     * @param pageIndex
     * @param pageSize
     * @param author_id
     * @return
     */
    @GetMapping("/getUserArticle")
    public ResponseEntity getUserArticle(Integer pageIndex,Integer pageSize,Integer author_id){
        return api_blog_article.getUserArticle(pageIndex,pageSize,author_id);
    }

    /**
     * 查询文章详情
     * @param contentId
     * @return
     */
    @GetMapping("/getArticleById")
    public ResponseEntity getArticleById(Integer contentId){
        if(contentId==null){
            return ResponseResult.Return(WebResultEmun.ARTICLE_DETAIL_NO_EXISTS);
        }
        return api_blog_article.getArticleById(contentId);
    };
}
