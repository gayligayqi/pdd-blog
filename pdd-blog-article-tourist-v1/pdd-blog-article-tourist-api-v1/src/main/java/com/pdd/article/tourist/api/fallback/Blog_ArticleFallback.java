package com.pdd.article.tourist.api.fallback;

import com.pdd.article.tourist.api.feign.API_Blog_Article;
import com.pdd.article.tourist.enums.WebResultEmun;
import com.pdd.article.tourist.response.ResponseResult;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.stereotype.Component;

/**
 * @author:liyangpeng
 * @date:2019/6/19 11:35
 */
@Component
public class Blog_ArticleFallback implements API_Blog_Article {

    @Override
    public ResponseEntity getArticle(Integer pageIndex, Integer pageSize) {
        return ResponseResult.Return(WebResultEmun.SERVER_INVOKE_ARTICLE_ERROR);
    }

    @Override
    public ResponseEntity getUserArticle(Integer pageIndex, Integer pageSize, Integer author_id) {
        return ResponseResult.Return(WebResultEmun.SERVER_INVOKE_USER_ARTICLE_ERROR);
    }

    @Override
    public ResponseEntity getArticleById(Integer contentId) {
        return ResponseResult.Return(WebResultEmun.SERVER_INVOKE_ARTICLE_DETAIL_ERROR);
    }
}
