package com.pdd.article.tourist.api.fallback;

import com.pdd.article.tourist.api.feign.API_Blog_Update_History;
import com.pdd.article.tourist.enums.WebResultEmun;
import com.pdd.article.tourist.response.ResponseResult;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.stereotype.Component;

/**
 * @author:liyangpeng
 * @date:2019/7/10 17:05
 */
@Component
public class Blog_Update_HistoryFallback implements API_Blog_Update_History {

    @Override
    public ResponseEntity getUpdateHistory() {
        return ResponseResult.Return(WebResultEmun.SERVER_INVOKE_UPDATE_HISTORY_ERROR);
    }
}
