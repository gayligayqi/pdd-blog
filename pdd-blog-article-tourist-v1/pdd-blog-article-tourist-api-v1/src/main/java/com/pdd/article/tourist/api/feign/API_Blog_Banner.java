package com.pdd.article.tourist.api.feign;

import com.pdd.article.tourist.api.fallback.Blog_BannerFallback;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author:liyangpeng
 * @date:2019/7/9 19:57
 */
@FeignClient(value = "pdd-blog-article-tourist-server-v1", path = "blog_banner", fallback = Blog_BannerFallback.class)
public interface API_Blog_Banner {

    /**
     * 查询Banner
     * @return
     */
    @GetMapping("/getBanner")
    ResponseEntity getBanner();
}
