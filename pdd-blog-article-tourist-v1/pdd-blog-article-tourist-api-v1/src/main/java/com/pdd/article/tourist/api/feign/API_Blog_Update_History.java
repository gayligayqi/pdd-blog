package com.pdd.article.tourist.api.feign;

import com.pdd.article.tourist.api.fallback.Blog_Update_HistoryFallback;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author:liyangpeng
 * @date:2019/7/10 17:05
 */
@FeignClient(value = "pdd-blog-article-tourist-server-v1", path = "blog_update_history", fallback = Blog_Update_HistoryFallback.class)
public interface API_Blog_Update_History {

    /**
     * 查询网站更新历史
     * @return
     */
    @GetMapping("/getUpdateHistory")
    ResponseEntity getUpdateHistory();

}
