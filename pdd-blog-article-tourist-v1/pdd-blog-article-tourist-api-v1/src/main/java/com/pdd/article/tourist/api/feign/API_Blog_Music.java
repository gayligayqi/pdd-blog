package com.pdd.article.tourist.api.feign;

import com.pdd.article.tourist.api.fallback.API_Blog_MusicFallback;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author:liyangpeng
 * @date:2019/8/20 14:33
 */
@FeignClient(value = "pdd-blog-article-tourist-server-v1", path = "blog_music", fallback = API_Blog_MusicFallback.class)
public interface API_Blog_Music {
    /**
     * 随机播放30首歌曲
     * @return
     */
    @GetMapping("/getRecommentMusic")
    ResponseEntity getRecommentMusic();
}
