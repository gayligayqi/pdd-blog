package com.pdd.article.tourist.api.fallback;

import com.pdd.article.tourist.api.feign.API_Blog_Music;
import com.pdd.article.tourist.enums.WebResultEmun;
import com.pdd.article.tourist.response.ResponseResult;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.stereotype.Component;

/**
 * @author:liyangpeng
 * @date:2019/8/20 14:33
 */
@Component
public class API_Blog_MusicFallback implements API_Blog_Music {

    @Override
    public ResponseEntity getRecommentMusic() {
        return ResponseResult.Return(WebResultEmun.SERVER_INVOKE_GET_RECOMMEND_MUSIC_ERROR);
    }
}
