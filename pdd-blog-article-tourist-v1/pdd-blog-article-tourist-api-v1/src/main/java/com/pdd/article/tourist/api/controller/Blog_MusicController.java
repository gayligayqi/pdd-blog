package com.pdd.article.tourist.api.controller;

import com.pdd.article.tourist.api.feign.API_Blog_Music;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author:liyangpeng
 * @date:2019/8/20 14:32
 */
@RestController
@RequestMapping("/blog_music")
public class Blog_MusicController {

    @Autowired
    private API_Blog_Music api_blog_music;

    /**
     * 随机播放30首歌
     * @return
     */
    @GetMapping("/getRecommentMusic")
    public ResponseEntity getRecommentMusic(){
        return api_blog_music.getRecommentMusic();
    }
}
