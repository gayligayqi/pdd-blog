package com.pdd.article.tourist.api.feign;

import com.pdd.article.tourist.api.fallback.Blog_ArticleFallback;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author:liyangpeng
 * @date:2019/6/19 11:35
 */
@FeignClient(value = "pdd-blog-article-tourist-server-v1", path = "blog_article", fallback = Blog_ArticleFallback.class)
public interface API_Blog_Article {

    /**
     * 查询列表文章接口
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @GetMapping("/getArticle")
    ResponseEntity getArticle(@RequestParam("pageIndex") Integer pageIndex, @RequestParam("pageSize") Integer pageSize);

    /**
     * 查询用户主页列表文章
     * @param pageIndex
     * @param pageSize
     * @param author_id
     * @return
     */
    @GetMapping("/getUserArticle")
    ResponseEntity getUserArticle(@RequestParam("pageIndex") Integer pageIndex,@RequestParam("pageSize") Integer pageSize, @RequestParam("author_id") Integer author_id);

    /**
     * 查询文章详情
     * @param contentId
     * @return
     */
    @GetMapping("/getArticleById")
    ResponseEntity getArticleById(@RequestParam("contentId") Integer contentId);
}
