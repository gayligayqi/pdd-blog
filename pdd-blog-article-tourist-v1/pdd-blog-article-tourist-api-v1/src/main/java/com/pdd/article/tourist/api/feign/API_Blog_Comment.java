package com.pdd.article.tourist.api.feign;

import com.pdd.article.tourist.api.fallback.Blog_CommentFallback;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author:liyangpeng
 * @date:2019/7/4 20:10
 */
@FeignClient(value = "pdd-blog-article-tourist-server-v1", path = "blog_comment", fallback = Blog_CommentFallback.class)
public interface API_Blog_Comment {

    /**
     * 查询评论的接口
     * @param pageIndex
     * @param pageSize
     * @param contentId
     * @return
     */
    @GetMapping("/getCommentByArticleId")
    ResponseEntity getCommentByArticleId(@RequestParam("pageIndex") Integer pageIndex, @RequestParam("pageSize") Integer pageSize, @RequestParam("contentId") Integer contentId);
}
