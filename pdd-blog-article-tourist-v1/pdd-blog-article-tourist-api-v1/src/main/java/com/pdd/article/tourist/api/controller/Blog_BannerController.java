package com.pdd.article.tourist.api.controller;

import com.pdd.article.tourist.api.feign.API_Blog_Banner;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author:liyangpeng
 * @date:2019/7/9 19:57
 */
@RestController
@RequestMapping("/blog_banner")
public class Blog_BannerController {

    @Autowired
    private API_Blog_Banner api_blog_banner;

    /**
     * 查询Banner
     * @return
     */
    @GetMapping("/getBanner")
    public ResponseEntity getBanner(){
        return api_blog_banner.getBanner();
    }
}
