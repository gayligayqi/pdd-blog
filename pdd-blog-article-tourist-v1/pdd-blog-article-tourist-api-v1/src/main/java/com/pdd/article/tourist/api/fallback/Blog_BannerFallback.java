package com.pdd.article.tourist.api.fallback;

import com.pdd.article.tourist.api.feign.API_Blog_Banner;
import com.pdd.article.tourist.enums.WebResultEmun;
import com.pdd.article.tourist.response.ResponseResult;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.stereotype.Component;

/**
 * @author:liyangpeng
 * @date:2019/7/9 19:57
 */
@Component
public class Blog_BannerFallback implements API_Blog_Banner {

    @Override
    public ResponseEntity getBanner() {
        return ResponseResult.Return(WebResultEmun.SERVER_INVOKE_BANNER_ERROR);
    }
}
