package com.pdd.article.tourist.api.controller;

import com.pdd.article.tourist.api.feign.API_Blog_Update_History;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author:liyangpeng
 * @date:2019/7/10 17:05
 */
@RestController
@RequestMapping("/blog_update_history")
public class Blog_Update_HistoryController {

    @Autowired
    private API_Blog_Update_History api_blog_update_history;

    /**
     * 查询网站更新历史
     * @return
     */
    @GetMapping("/getUpdateHistory")
    public ResponseEntity getUpdateHistory(){
        return api_blog_update_history.getUpdateHistory();
    }

}
