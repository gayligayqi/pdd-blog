package com.pdd.article.tourist.api.fallback;

import com.pdd.article.tourist.api.feign.API_Blog_Comment;
import com.pdd.article.tourist.enums.WebResultEmun;
import com.pdd.article.tourist.response.ResponseResult;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.stereotype.Component;

/**
 * @author:liyangpeng
 * @date:2019/7/4 20:10
 */
@Component
public class Blog_CommentFallback implements API_Blog_Comment {

    @Override
    public ResponseEntity getCommentByArticleId(Integer pageIndex, Integer pageSize, Integer contentId) {
        return ResponseResult.Return(WebResultEmun.SERVER_INVOKE_COMMENT_ERROR);
    }
}
