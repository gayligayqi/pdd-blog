package com.pdd.article.tourist.enums;

/**
 * @author:liyangpeng
 * @date:2019/6/19 13:54
 */
public enum WebResultEmun {
    SUCCESS("PBAT_0000","请求成功",true),
    ARTICLE_DETAIL_NO_REVIEW("PBAT_D0001","内容审核中",false),
    ARTICLE_DETAIL_NO_EXISTS("PBAT_D0002","内容不存在",false),
    ARTICLE_DETAIL_IS_DELETE("PBAT_D0003","内容已被删除",false),
    SERVER_INVOKE_ARTICLE_ERROR("PBAT_SIE0001","获取文章失败,错误码:PBAT_SIE0001",false),
    SERVER_INVOKE_USER_ARTICLE_ERROR("PBAT_SIE0002","获取文章失败,错误码:PBAT_SIE0002",false),
    SERVER_INVOKE_ARTICLE_DETAIL_ERROR("PBAT_SIE0003","获取文章详情失败,错误码:PBAT_SIE0003",false),
    SERVER_INVOKE_BANNER_ERROR("PBAT_SIE0004","获取轮播数据失败,错误码:PBAT_SIE0004",false),
    SERVER_INVOKE_COMMENT_ERROR("PBAT_SIE0005","获取评论失败,错误码:PBAT_SIE0005",false),
    SERVER_INVOKE_UPDATE_HISTORY_ERROR("PBAT_SIE0006","获取小站里程碑数据失败,错误码:PBAT_SIE0006",false),
    SERVER_INVOKE_GET_RECOMMEND_MUSIC_ERROR("PBAT_SIE0007","获取推荐音乐失败,错误码:PBAT_SIE0007",false)

    ;

    private String code;
    private String msg;
    private boolean success;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    WebResultEmun(String code, String msg, boolean success) {
        this.code = code;
        this.msg = msg;
        this.success=success;
    }
}
