package com.pdd.article.tourist.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pdd.commons.entity.dao.Blog_Comment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author:liyangpeng
 * @date:2019/7/4 20:09
 */
@Mapper
public interface Blog_CommentMapper extends BaseMapper<Blog_Comment> {

    IPage<Blog_Comment> getCommentByArticleId(IPage<Blog_Comment> page, @Param("articleId") Integer articleId, @Param("author_id") Integer author_id);

}
