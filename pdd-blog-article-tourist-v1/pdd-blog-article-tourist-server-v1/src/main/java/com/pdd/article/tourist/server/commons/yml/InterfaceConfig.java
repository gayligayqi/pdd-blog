package com.pdd.article.tourist.server.commons.yml;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author:liyangpeng
 * @date:2019/6/20 15:01
 */
@Data
@Component
@ConfigurationProperties(prefix = "interface")
public class InterfaceConfig {
    /**
     * 正文内容a标签外链
     */
    private String contentLink;
    /**
     * 百度获取ip信息接口
     */
    private String baiduIp;

}
