package com.pdd.article.tourist.server.controller;

import com.pdd.article.tourist.api.feign.API_Blog_Music;
import com.pdd.article.tourist.server.service.Blog_MusicService;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author:liyangpeng
 * @date:2019/8/20 14:32
 */
@RestController
@RequestMapping("/blog_music")
public class Blog_MusicController implements API_Blog_Music{

    @Autowired
    private Blog_MusicService blog_musicService;

    /**
     * 获取随机推荐30首歌曲
     * @return
     */
    @Override
    public ResponseEntity getRecommentMusic() {
        return blog_musicService.getRecommentMusic();
    }
}
