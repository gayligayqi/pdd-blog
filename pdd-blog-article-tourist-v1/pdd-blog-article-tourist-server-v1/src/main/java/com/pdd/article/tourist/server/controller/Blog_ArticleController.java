package com.pdd.article.tourist.server.controller;

import com.pdd.article.tourist.api.feign.API_Blog_Article;
import com.pdd.article.tourist.enums.WebResultEmun;
import com.pdd.article.tourist.response.ResponseResult;
import com.pdd.article.tourist.server.service.Blog_ArticleService;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author:liyangpeng
 * @date:2019/6/19 11:35
 */
@RestController
@RequestMapping("/blog_article")
public class Blog_ArticleController implements API_Blog_Article {

    @Autowired
    private Blog_ArticleService blog_articleService;

    /**
     * 查询列表文章接口
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @Override
    public ResponseEntity getArticle(Integer pageIndex, Integer pageSize){
        return blog_articleService.getAllArticle(pageIndex,pageSize);
    }

    /**
     * 查询用户主页列表文章
     * @param pageIndex
     * @param pageSize
     * @param author_id
     * @return
     */
    @Override
    public ResponseEntity getUserArticle(Integer pageIndex,Integer pageSize,Integer author_id){
        return blog_articleService.getUserArticle(pageIndex,pageSize,author_id);
    }

    /**
     * 查询文章详情
     * @param contentId
     * @return
     */
    @Override
    public ResponseEntity getArticleById(Integer contentId){
        if(contentId==null){
            return ResponseResult.Return(WebResultEmun.ARTICLE_DETAIL_NO_EXISTS);
        }
        return blog_articleService.getArticleById(contentId);
    };
}
