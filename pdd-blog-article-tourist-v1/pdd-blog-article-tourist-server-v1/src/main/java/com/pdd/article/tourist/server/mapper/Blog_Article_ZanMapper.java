package com.pdd.article.tourist.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pdd.commons.entity.dao.Blog_Article_Zan;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author:liyangpeng
 * @date:2019/7/9 9:41
 */
@Mapper
public interface Blog_Article_ZanMapper extends BaseMapper<Blog_Article_Zan>  {

}
