package com.pdd.article.tourist.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pdd.commons.entity.dao.Blog_Article;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author:liyangpeng
 * @date:2019/6/19 11:20
 */
@Mapper
public interface Blog_ArticleMapper extends BaseMapper<Blog_Article> {

    IPage<Blog_Article> selectPage(IPage<Blog_Article> page);

    IPage<Blog_Article> selectUserListPage(IPage<Blog_Article> page, @Param("author_id") Integer author_id);

    Blog_Article getArticleById(Integer id);
}
