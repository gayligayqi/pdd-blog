package com.pdd.article.tourist.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author:liyangpeng
 * @date:2019/7/11 11:13
 */
@SpringBootApplication(scanBasePackages = {"com.pdd.article.tourist.server","com.pdd.commons"},excludeName ="com.pdd.article.tourist.api")
@EnableEurekaClient
public class PddBlogArticleTouristServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(PddBlogArticleTouristServerApplication.class,args);
    }
}
