package com.pdd.article.tourist.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.pdd.article.tourist.response.ResponseResult;
import com.pdd.article.tourist.server.mapper.Blog_BannerMapper;
import com.pdd.article.tourist.server.service.Blog_BannerService;
import com.pdd.commons.entity.dao.Blog_Banner;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author:liyangpeng
 * @date:2019/7/9 19:56
 */
@Service
public class Blog_BannerServiceImpl implements Blog_BannerService {

    @Autowired
    private Blog_BannerMapper blog_bannerMapper;

    @Override
    public ResponseEntity getBanner() {
        QueryWrapper<Blog_Banner> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("isdel","0");
        List<Blog_Banner> bannerList=blog_bannerMapper.selectList(queryWrapper);
        return ResponseResult.Return(bannerList);
    }
}
