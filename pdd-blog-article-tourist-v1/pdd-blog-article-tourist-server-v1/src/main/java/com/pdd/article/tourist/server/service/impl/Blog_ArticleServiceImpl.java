package com.pdd.article.tourist.server.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pdd.article.tourist.enums.WebResultEmun;
import com.pdd.article.tourist.response.ResponseResult;
import com.pdd.article.tourist.server.mapper.Blog_ArticleMapper;
import com.pdd.article.tourist.server.service.Blog_ArticleService;
import com.pdd.article.tourist.server.service.Blog_Article_CollectionService;
import com.pdd.article.tourist.server.service.Blog_Article_ZanService;
import com.pdd.commons.entity.dao.Blog_Article;
import com.pdd.commons.entity.dao.Blog_Article_Collection;
import com.pdd.commons.entity.dao.Blog_Article_Zan;
import com.pdd.commons.entity.vo.AccountInfo_Vo;
import com.pdd.commons.entity.vo.Article_Vo;
import com.pdd.commons.response.ResponseEntity;
import com.pdd.commons.utils.AccountUtil;
import com.pdd.commons.utils.PublicKey;
import com.pdd.commons.utils.RedisUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.util.HtmlUtils;

/**
 * @author:liyangpeng
 * @date:2019/6/19 11:21
 */
@Service
@Slf4j
public class Blog_ArticleServiceImpl implements Blog_ArticleService {

    @Autowired
    private Blog_ArticleMapper blog_articleMapper;

    @Autowired
    private AccountUtil accountUtil;

    @Autowired
    private RedisUtils redisUtils;
    @Autowired
    private Blog_Article_CollectionService blog_article_collectionService;
    @Autowired
    private Blog_Article_ZanService blog_article_zanService;

    @Override
    public ResponseEntity getAllArticle(Integer pageIndex, Integer pageSize){
        Page<Blog_Article> page=new Page<>(pageIndex==null?0:pageIndex,pageSize==null?10:pageSize);
        IPage<Blog_Article> blog_articleIPage=blog_articleMapper.selectPage(page);
        return ResponseResult.Return(blog_articleIPage);
    }


    @Override
    public ResponseEntity getUserArticle(Integer pageIndex, Integer pageSize, Integer author_id) {
        Page<Blog_Article> page=new Page<>(pageIndex==null?0:pageIndex,pageSize==null?10:pageSize);
        IPage<Blog_Article> blog_articleIPage=blog_articleMapper.selectUserListPage(page,author_id);
        return ResponseResult.Return(blog_articleIPage);
    }

    @Override
    public ResponseEntity getArticleById(Integer id) {
        Blog_Article blog_article=blog_articleMapper.getArticleById(id);
        if(blog_article==null||blog_article.getIspass()==2){
            return ResponseResult.Return(WebResultEmun.ARTICLE_DETAIL_NO_EXISTS);
        }
        if(blog_article.getIspass()==0){
            return ResponseResult.Return(WebResultEmun.ARTICLE_DETAIL_NO_REVIEW);
        }
        if(blog_article.getIsdel()==1){
            return ResponseResult.Return(WebResultEmun.ARTICLE_DETAIL_IS_DELETE);
        }
        //获取用户登录信息
        AccountInfo_Vo accountInfo_vo=accountUtil.getLoginInfo();
        if(accountInfo_vo!=null){
            //查询收藏
            Blog_Article_Collection blog_article_collection=blog_article_collectionService.isColl(id,accountInfo_vo.getId());
            blog_article.setIsColl(blog_article_collection==null?0:blog_article_collection.getCollStatus());
            //查询点赞
            Blog_Article_Zan blog_article_zan=blog_article_zanService.isZan(id,accountInfo_vo.getId());
            blog_article.setIsZan(blog_article_zan==null?0:blog_article_zan.getClickStatus());
        }
        String Result=String.valueOf(redisUtils.hashGet(PublicKey.ARTICLE_DATA_MAP,blog_article.getCacheKey()));
        Article_Vo article_vo= JSON.parseObject(Result, Article_Vo.class);
        blog_article.setContent(HtmlUtils.htmlUnescape(article_vo.getContent()));
        return ResponseResult.Return(blog_article);
    }
}
