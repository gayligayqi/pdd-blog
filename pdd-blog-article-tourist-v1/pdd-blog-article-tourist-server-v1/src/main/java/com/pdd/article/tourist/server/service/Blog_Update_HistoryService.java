package com.pdd.article.tourist.server.service;

import com.pdd.commons.response.ResponseEntity;

/**
 * @author:liyangpeng
 * @date:2019/7/10 17:01
 */
public interface Blog_Update_HistoryService {

    ResponseEntity getUpdateHistory();

}
