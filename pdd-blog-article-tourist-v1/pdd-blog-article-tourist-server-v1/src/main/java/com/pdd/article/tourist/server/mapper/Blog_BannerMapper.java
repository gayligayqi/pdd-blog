package com.pdd.article.tourist.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pdd.commons.entity.dao.Blog_Banner;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author:liyangpeng
 * @date:2019/7/9 19:56
 */
@Mapper
public interface Blog_BannerMapper  extends BaseMapper<Blog_Banner> {

}
