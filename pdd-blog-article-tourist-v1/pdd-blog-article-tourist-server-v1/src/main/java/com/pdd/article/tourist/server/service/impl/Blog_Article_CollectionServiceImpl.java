package com.pdd.article.tourist.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.pdd.article.tourist.server.mapper.Blog_Article_CollectionMapper;
import com.pdd.article.tourist.server.service.Blog_Article_CollectionService;
import com.pdd.commons.entity.dao.Blog_Article_Collection;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author:liyangpeng
 * @date:2019/7/9 10:34
 */
@Service
@Slf4j
public class Blog_Article_CollectionServiceImpl implements Blog_Article_CollectionService {

    @Autowired
    private Blog_Article_CollectionMapper blog_article_collectionMapper;

    @Override
    public Blog_Article_Collection isColl(Integer articleId, Integer author_id) {
        QueryWrapper<Blog_Article_Collection> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("author_id",author_id);
        queryWrapper.eq("articleId",articleId);
        Blog_Article_Collection blog_article_collection=blog_article_collectionMapper.selectOne(queryWrapper);
        return blog_article_collection;
    }
}
