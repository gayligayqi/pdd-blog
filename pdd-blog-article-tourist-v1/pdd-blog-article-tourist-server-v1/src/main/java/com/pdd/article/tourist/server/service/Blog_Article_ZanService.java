package com.pdd.article.tourist.server.service;

import com.pdd.commons.entity.dao.Blog_Article_Zan;

/**
 * @author:liyangpeng
 * @date:2019/7/9 9:42
 */
public interface Blog_Article_ZanService {

    Blog_Article_Zan isZan(Integer articleId, Integer author_id);
}
