package com.pdd.article.tourist.server.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pdd.article.tourist.response.ResponseResult;
import com.pdd.article.tourist.server.mapper.Blog_CommentMapper;
import com.pdd.article.tourist.server.service.Blog_CommentService;
import com.pdd.commons.entity.dao.Blog_Comment;
import com.pdd.commons.entity.vo.AccountInfo_Vo;
import com.pdd.commons.response.ResponseEntity;
import com.pdd.commons.utils.AccountUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author:liyangpeng
 * @date:2019/7/4 20:10
 */
@Service
@Slf4j
public class    Blog_CommentServiceImpl implements Blog_CommentService {

    @Autowired
    private Blog_CommentMapper blog_commentMapper;

    @Autowired
    private AccountUtil accountUtil;


    @Override
    public ResponseEntity getCommentByArticleId(Integer pageIndex, Integer pageSize, Integer articleId) {
        Page<Blog_Comment> page=new Page<>(pageIndex==null?0:pageIndex,pageSize==null?10:pageSize);
        AccountInfo_Vo accountInfo_vo=accountUtil.getLoginInfo();
        Integer author_Id=0;
        if(accountInfo_vo!=null){
            author_Id=accountInfo_vo.getId();
        }
        IPage<Blog_Comment> iPage=blog_commentMapper.getCommentByArticleId(page,articleId,author_Id);
        return ResponseResult.Return(iPage);
    }
}
