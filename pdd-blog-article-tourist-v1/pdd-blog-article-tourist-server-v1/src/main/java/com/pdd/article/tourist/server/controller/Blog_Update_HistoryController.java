package com.pdd.article.tourist.server.controller;

import com.pdd.article.tourist.api.feign.API_Blog_Update_History;
import com.pdd.article.tourist.server.service.Blog_Update_HistoryService;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author:liyangpeng
 * @date:2019/7/10 17:05
 */
@RestController
@RequestMapping("/blog_update_history")
public class Blog_Update_HistoryController implements API_Blog_Update_History {

    @Autowired
    private Blog_Update_HistoryService blog_update_historyService;

    /**
     * 查询网站更新历史
     * @return
     */
    @Override
    public ResponseEntity getUpdateHistory(){
        return blog_update_historyService.getUpdateHistory();
    }

}
