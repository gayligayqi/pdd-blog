package com.pdd.article.tourist.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pdd.commons.entity.dao.Blog_Article_Collection;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author:liyangpeng
 * @date:2019/7/9 10:33
 */
@Mapper
public interface Blog_Article_CollectionMapper extends BaseMapper<Blog_Article_Collection> {

}