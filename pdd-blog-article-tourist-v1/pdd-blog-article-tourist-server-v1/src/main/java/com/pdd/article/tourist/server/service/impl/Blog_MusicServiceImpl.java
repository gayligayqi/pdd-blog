package com.pdd.article.tourist.server.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pdd.article.tourist.response.ResponseResult;
import com.pdd.article.tourist.server.mapper.Blog_MusicMapper;
import com.pdd.article.tourist.server.service.Blog_MusicService;
import com.pdd.commons.entity.dao.Blog_Music;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author:liyangpeng
 * @date:2019/8/20 14:41
 */
@Service
public class Blog_MusicServiceImpl implements Blog_MusicService {

    @Autowired
    private Blog_MusicMapper blog_musicMapper;

    /**
     * 随机推荐30首歌曲
     * @return
     */
    @Override
    public ResponseEntity getRecommentMusic() {
        IPage<Blog_Music> iPage=blog_musicMapper.selectPage(new Page<>(1,30), Wrappers.<Blog_Music>query().eq("isdel",0).orderByDesc("RAND()"));
        return ResponseResult.Return(iPage);
    }
}
