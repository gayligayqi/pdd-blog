package com.pdd.article.tourist.server.controller;

import com.pdd.article.tourist.api.feign.API_Blog_Banner;
import com.pdd.article.tourist.server.service.Blog_BannerService;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author:liyangpeng
 * @date:2019/7/9 19:57
 */
@RestController
@RequestMapping("/blog_banner")
public class Blog_BannerController implements API_Blog_Banner {

    @Autowired
    private Blog_BannerService blog_bannerService;

    /**
     * 查询Banner
     * @return
     */
    @Override
    public ResponseEntity getBanner(){
        return blog_bannerService.getBanner();
    }
}
