package com.pdd.article.tourist.server.service;

import com.pdd.commons.response.ResponseEntity;

/**
 * @author:liyangpeng
 * @date:2019/7/4 20:10
 */
public interface Blog_CommentService {

    ResponseEntity getCommentByArticleId(Integer pageIndex, Integer pageSize, Integer articleId);
}
