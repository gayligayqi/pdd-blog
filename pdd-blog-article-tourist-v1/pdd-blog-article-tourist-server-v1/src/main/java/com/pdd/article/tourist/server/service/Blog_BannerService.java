package com.pdd.article.tourist.server.service;

import com.pdd.commons.response.ResponseEntity;

/**
 * @author:liyangpeng
 * @date:2019/7/9 19:56
 */
public interface Blog_BannerService {

    ResponseEntity getBanner();
}
