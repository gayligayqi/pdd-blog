package com.pdd.article.tourist.server.service;

import com.pdd.commons.entity.dao.Blog_Article_Collection;

/**
 * @author:liyangpeng
 * @date:2019/7/9 10:33
 */
public interface Blog_Article_CollectionService {

    Blog_Article_Collection isColl(Integer articleId, Integer author_id);
}
