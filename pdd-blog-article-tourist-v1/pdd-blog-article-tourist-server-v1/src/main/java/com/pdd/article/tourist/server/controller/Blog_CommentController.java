package com.pdd.article.tourist.server.controller;

import com.pdd.article.tourist.api.feign.API_Blog_Comment;
import com.pdd.article.tourist.response.ResponseResult;
import com.pdd.article.tourist.server.service.Blog_CommentService;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author:liyangpeng
 * @date:2019/7/4 20:10
 */
@RestController
@RequestMapping("/blog_comment")
public class Blog_CommentController implements API_Blog_Comment {

    @Autowired
    private Blog_CommentService blog_commentService;

    /**
     * 查询评论的接口
     * @param pageIndex
     * @param pageSize
     * @param contentId
     * @return
     */
    @Override
    public ResponseEntity getCommentByArticleId(Integer pageIndex, Integer pageSize, Integer contentId){
        if(contentId==null){
            return ResponseResult.RETURN_ERROR("参数错误");
        }
        return blog_commentService.getCommentByArticleId(pageIndex,pageSize,contentId);
    }
}
