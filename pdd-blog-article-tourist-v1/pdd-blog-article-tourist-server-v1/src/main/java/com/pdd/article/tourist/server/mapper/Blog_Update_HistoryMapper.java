package com.pdd.article.tourist.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pdd.commons.entity.dao.Blog_Update_History;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author:liyangpeng
 * @date:2019/7/10 17:02
 */
@Mapper
public interface Blog_Update_HistoryMapper extends BaseMapper<Blog_Update_History> {

}
