package com.pdd.article.tourist.server.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pdd.article.tourist.response.ResponseResult;
import com.pdd.article.tourist.server.mapper.Blog_Update_HistoryMapper;
import com.pdd.article.tourist.server.service.Blog_Update_HistoryService;
import com.pdd.commons.entity.dao.Blog_Update_History;
import com.pdd.commons.response.ResponseEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author:liyangpeng
 * @date:2019/7/10 17:01
 */
@Service
@Slf4j
public class Blog_Update_HistoryServiceImpl implements Blog_Update_HistoryService {

    @Autowired
    private Blog_Update_HistoryMapper blog_update_historyMapper;

    @Override
    public ResponseEntity getUpdateHistory() {
        IPage<Blog_Update_History> result=blog_update_historyMapper.selectPage(new Page<>(0,8),null);
        return ResponseResult.Return(result);
    }
}
