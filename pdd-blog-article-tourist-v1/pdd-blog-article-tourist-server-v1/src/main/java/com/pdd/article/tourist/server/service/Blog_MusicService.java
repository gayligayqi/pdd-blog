package com.pdd.article.tourist.server.service;

import com.pdd.commons.response.ResponseEntity;

/**
 * @author:liyangpeng
 * @date:2019/8/20 14:40
 */
public interface Blog_MusicService {

    /**
     * 随机推荐30首歌曲
     * @return
     */
    ResponseEntity getRecommentMusic();
}
