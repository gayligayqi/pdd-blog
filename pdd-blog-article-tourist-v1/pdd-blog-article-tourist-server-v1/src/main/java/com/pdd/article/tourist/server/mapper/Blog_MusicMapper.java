package com.pdd.article.tourist.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pdd.commons.entity.dao.Blog_Music;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author:liyangpeng
 * @date:2019/8/20 14:41
 */
@Mapper
public interface Blog_MusicMapper extends BaseMapper<Blog_Music> {

}
