package com.pdd.article.tourist.server.service;

import com.pdd.commons.response.ResponseEntity;

/**
 * @author:liyangpeng
 * @date:2019/6/19 11:21
 */
public interface Blog_ArticleService {

    ResponseEntity getAllArticle(Integer pageIndex, Integer pageSize);

    ResponseEntity getUserArticle(Integer pageIndex, Integer pageSize, Integer author_id);

    ResponseEntity getArticleById(Integer id);
}
