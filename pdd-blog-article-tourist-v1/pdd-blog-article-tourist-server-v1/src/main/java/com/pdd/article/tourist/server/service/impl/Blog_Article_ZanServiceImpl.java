package com.pdd.article.tourist.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.pdd.article.tourist.server.mapper.Blog_Article_ZanMapper;
import com.pdd.article.tourist.server.service.Blog_Article_ZanService;
import com.pdd.commons.entity.dao.Blog_Article_Zan;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author:liyangpeng
 * @date:2019/7/9 9:43
 */
@Service
@Slf4j
public class Blog_Article_ZanServiceImpl implements Blog_Article_ZanService {

    @Autowired
    private Blog_Article_ZanMapper blog_article_zanMapper;

    @Override
    public Blog_Article_Zan isZan(Integer articleId, Integer author_id) {
        QueryWrapper<Blog_Article_Zan> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("author_id",author_id);
        queryWrapper.eq("articleId",articleId);
        Blog_Article_Zan blog_article_zan=blog_article_zanMapper.selectOne(queryWrapper);
        return blog_article_zan;
    }
}

