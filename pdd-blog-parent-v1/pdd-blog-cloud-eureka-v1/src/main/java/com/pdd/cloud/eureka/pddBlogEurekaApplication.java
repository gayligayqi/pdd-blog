package com.pdd.cloud.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author:liyangpeng
 * @date:2019/7/2 16:10
 */
@SpringBootApplication
@EnableEurekaServer
public class pddBlogEurekaApplication {
    public static void main(String[] args) {
        SpringApplication.run(pddBlogEurekaApplication.class, args);
    }
}
