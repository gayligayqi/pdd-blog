package com.pdd.cloud.dashboard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

/**
 * @author:liyangpeng
 * @date:2019/8/23 17:35
 */
@SpringBootApplication
@EnableHystrixDashboard
public class PddBlodDashBoardApplication {

    public static void main(String[] args) {
        SpringApplication.run(PddBlodDashBoardApplication.class,args);
    }
}
