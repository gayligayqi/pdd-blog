package com.pdd.cloud.zuul.enums;

import org.springframework.http.HttpStatus;

/**
 * @author:liyangpeng
 * @date:2019/7/3 9:24
 */
public enum  HttpResponse {
    TIME_OUT(HttpStatus.OK,"服务器打瞌睡了,请稍后再试哦!"),
    CLIENT_ERROR(HttpStatus.OK,"服务器堵车中,emmmmmmmm!")
    ;
    private HttpStatus status;
    private String message;

    HttpResponse(HttpStatus status, String s) {
        this.setStatus(status);
        this.setMessage(s);
    }

    public HttpStatus getStatus() {
        return status;
    }

    private void setStatus(HttpStatus status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    private void setMessage(String message) {
        this.message = message;
    }
}
