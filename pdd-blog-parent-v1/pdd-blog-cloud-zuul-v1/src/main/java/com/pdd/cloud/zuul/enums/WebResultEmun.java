package com.pdd.cloud.zuul.enums;

/**
 * @author:liyangpeng
 * @date:2019/7/11 16:26
 */
public enum WebResultEmun {
    LOGIN_INFO_NOT_EXISTS("0001","登录信息已失效,请重新登录,错误编码:0001",false)
    ;

    private String code;
    private String msg;
    private boolean success;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    WebResultEmun(String code, String msg, boolean success) {
        this.code = code;
        this.msg = msg;
        this.success=success;
    }
}
