package com.pdd.message.api.controller;

import com.alibaba.fastjson.JSONObject;
import com.pdd.commons.entity.dto.SendMessageFrom;
import com.pdd.commons.response.ResponseEntity;
import com.pdd.message.api.feign.API_Blog_Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author:liyangpeng
 * @date:2019/7/30 14:46
 */
@RestController
@RequestMapping("/blog_message")
public class Blog_MessageController {

    @Autowired
    private API_Blog_Message api_blog_message;

    /**
     * 服务列表查询
     * @return
     */
    @GetMapping("/getTcpServerList")
    public ResponseEntity getTcpServerList(){
        return api_blog_message.getTcpServerList();
    }
    /**
     * 发送消息
     * @param sendMessageFrom
     * @return
     */
    @PostMapping("/sendMessage")
    public ResponseEntity sendMessage(@RequestBody @Valid SendMessageFrom sendMessageFrom){
        return api_blog_message.sendMessage(sendMessageFrom);
    };
    /**
     * 查询查询聊天记录
     */
    @GetMapping("/getMessageHistory")
    public ResponseEntity getMessageHistory(Integer pageIndex,Integer pageSize,Integer author_id){
        return api_blog_message.getMessageHistory(pageIndex,pageSize,author_id);
    };

    /**
     * 查询聊天列表
     * @return
     */
    @GetMapping("/getMessageList")
    public ResponseEntity getMessageList(){
        return api_blog_message.getMessageList();
    }

    /**
     * 删除聊天窗口
     * @return
     */
    @PostMapping("/delMessageList")
    public ResponseEntity delMessageList(@RequestBody JSONObject jsonObject){
        return api_blog_message.delMessageList(jsonObject);
    }
    /**
     * 创建聊天窗口
     * @return
     */
    @PostMapping("/createMessageBox")
    public ResponseEntity createMessageBox(@RequestBody JSONObject jsonObject){
        return api_blog_message.createMessageBox(jsonObject);
    };
}
