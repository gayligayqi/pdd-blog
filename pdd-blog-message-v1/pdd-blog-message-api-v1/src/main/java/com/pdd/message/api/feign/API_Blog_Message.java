package com.pdd.message.api.feign;

import com.alibaba.fastjson.JSONObject;
import com.pdd.commons.entity.dto.SendMessageFrom;
import com.pdd.commons.response.ResponseEntity;
import com.pdd.message.api.fallback.API_Blog_MessageFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * @author:liyangpeng
 * @date:2019/7/30 14:47
 */
@FeignClient(value = "pdd-blog-messgae-server-v1",path = "blog_message",fallback = API_Blog_MessageFallback.class)
public interface API_Blog_Message {
    /**
     * 当前可用的TCP消息推送服务列表
     * @return
     */
    @GetMapping("/getTcpServerList")
    ResponseEntity getTcpServerList();

    /**
     * 发送消息
     * @param sendMessageFrom
     * @return
     */
    @PostMapping("/sendMessage")
    ResponseEntity sendMessage(@RequestBody SendMessageFrom sendMessageFrom);

    /**
     * 查询消息历史记录
     * @param pageIndex
     * @param pageSize
     * @param author_id
     * @return
     */
    @GetMapping("/getMessageHistory")
    ResponseEntity getMessageHistory(@RequestParam("pageIndex") Integer pageIndex, @RequestParam("pageSize") Integer pageSize, @RequestParam("author_id") Integer author_id);

    /**
     * 查询聊天列表
     * @return
     */
    @GetMapping("/getMessageList")
    ResponseEntity getMessageList();

    /**
     * 删除聊天窗口
     * @return
     */
    @PostMapping("/delMessageList")
    ResponseEntity delMessageList(@RequestBody JSONObject jsonObject);
    /**
     * 创建聊天窗口
     * @return
     */
    @PostMapping("/createMessageBox")
    ResponseEntity createMessageBox(@RequestBody JSONObject jsonObject);
}
