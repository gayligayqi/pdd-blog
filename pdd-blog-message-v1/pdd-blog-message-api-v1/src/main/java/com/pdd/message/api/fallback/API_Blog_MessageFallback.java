package com.pdd.message.api.fallback;

import com.alibaba.fastjson.JSONObject;
import com.pdd.commons.entity.dto.SendMessageFrom;
import com.pdd.commons.response.ResponseEntity;
import com.pdd.message.api.feign.API_Blog_Message;
import com.pdd.message.enums.WebResultEmun;
import com.pdd.message.response.ResponseResult;
import org.springframework.stereotype.Component;

/**
 * @author:liyangpeng
 * @date:2019/7/30 14:48
 */
@Component
public class API_Blog_MessageFallback implements API_Blog_Message {

    @Override
    public ResponseEntity getTcpServerList() {
        return ResponseResult.Return(WebResultEmun.SERVER_GET_TCP_SERVER_LIST_INVOKE_ERROR);
    }

    @Override
    public ResponseEntity sendMessage(SendMessageFrom sendMessageFrom) {
        return ResponseResult.Return(WebResultEmun.SERVER_SEND_MESSAGE_ERROR);
    }

    @Override
    public ResponseEntity getMessageHistory(Integer pageIndex, Integer pageSize, Integer author_id) {
        return ResponseResult.Return(WebResultEmun.SERVER_MESSAGE_HISTORY_QUERY_ERROR);
    }

    @Override
    public ResponseEntity getMessageList() {
        return ResponseResult.Return(WebResultEmun.SERVER_GET_MESSAGE_LIST_ERROR);
    }

    @Override
    public ResponseEntity delMessageList(JSONObject jsonObject) {
        return ResponseResult.Return(WebResultEmun.SERVER_DEL_MESSAGE_LIST_ERROR);
    }

    @Override
    public ResponseEntity createMessageBox(JSONObject jsonObject) {
        return ResponseResult.Return(WebResultEmun.SERVER_CREATE_MESSAGE_BOX_ERROR);
    }
}
