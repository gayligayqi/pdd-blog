package com.pdd.message.enums;

/**
 * @author:liyangpeng
 * @date:2019/7/11 16:26
 */
public enum WebResultEmun {
    LOGIN_INFO_NOT_EXISTS("0001","登录信息已失效,请重新登录,错误编码:PBM_0001",false),
    SUCCESS("PBM_0000","请求成功",true),
    TCP_SERVER_LIST_EMPTY_ERROR("PBM_T0001","未找到可用的消息推送服务列表",false),
    SEND_MESSAGE_SUCCESS("PBM_S0001","发送成功",true),
    SEND_MESSAGE_ERROR("PBM_S0002","自己不能给自己发送消息",false),
    MESSAGE_CONTENT_EMPTY_ERROR("PBM_M0001","消息内容不能全为空格",false),
    DEL_MESSAGE_LIST_SUCCESS("PBM_DML0001","删除聊天窗口成功",true),
    DEL_MESSAGE_LIST_ERROR("PBM_DML0002","删除聊天窗口失败",false),
    CREATE_MESSAGE_BOX_SUCCESS("PBM_CMB0001","创建聊天窗口成功",true),
    CREATE_MESSAGE_BOX_ERROR("PBM_CMB0002","创建聊天窗口失败",false),
    SERVER_GET_TCP_SERVER_LIST_INVOKE_ERROR("PBM_SIE0001","获取服务列表失败,请刷新页面重新获取,错误码:PBM_SIE0001",false),
    SERVER_SEND_MESSAGE_ERROR("PBM_SIE0002","消息发送失败,错误码:PBM_SIE0002",false),
    SERVER_MESSAGE_HISTORY_QUERY_ERROR("PBM_SIE0003","查询消息历史记录失败,错误码:PBM_SIE0003",false),
    SERVER_GET_MESSAGE_LIST_ERROR("PBM_SIE0004","获取消息列表失败,错误码:PBM_SIE0004",false),
    SERVER_DEL_MESSAGE_LIST_ERROR("PBM_SIE0005","移除聊天窗口失败,错误码:PBM_SIE0005",false),
    SERVER_CREATE_MESSAGE_BOX_ERROR("PBM_SIE0006","创建聊天窗口失败,错误码:PBM_SIE0006",false)
    ;

    private String code;
    private String msg;
    private boolean success;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    WebResultEmun(String code, String msg, boolean success) {
        this.code = code;
        this.msg = msg;
        this.success=success;
    }
}
