package com.pdd.message.server.commons.utils;

import com.pdd.commons.entity.vo.Message_User_Vo;
import com.pdd.commons.entity.vo.Message_Vo;
import java.util.Date;

/**
 * @author:liyangpeng
 * @date:2019/8/9 11:23
 */
public class SystemMessageUtil {

    /**
     * 头像链接
     */
    private final static String AVATAR="http://00-upload.gayligayqi.com/blog/avatar/soz.png";
    /**
     * 昵称
     */
    private final static String NICKNAME="系统消息";
    /**
     * 未读消息通知
     */
    private final static String UNREAD_TEXT="您有%d条未读消息";
    /**
     * 系统消息
     * 生成系统用户信息
     */
    private static Message_User_Vo getSystemUser(Integer choseboxId){
        Message_User_Vo message_User_vo =new Message_User_Vo(choseboxId,NICKNAME,AVATAR,1);
        return message_User_vo;
    };

    /**
     * 生成未读消息通知
     * @return
     */
    public static Message_Vo getUnReadMessage(Integer choseBoxId,Long size){
        Message_Vo message_vo=new Message_Vo();
        message_vo.setContent(String.format(UNREAD_TEXT,size));
        message_vo.setSelf(0);
        message_vo.setUser_vo(getSystemUser(choseBoxId));
        message_vo.setTimeStamp(new Date());
        return message_vo;
    }
}
