package com.pdd.message.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pdd.commons.entity.dao.Blog_Message_List;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author:liyangpeng
 * @date:2019/8/12 11:41
 */
@Mapper
public interface Blog_Message_ListMapper extends BaseMapper<Blog_Message_List> {

    List<Blog_Message_List> selectList(Integer msg_author_id);
}
