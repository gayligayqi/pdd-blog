package com.pdd.message.server.controller;

import com.alibaba.fastjson.JSONObject;
import com.pdd.commons.entity.dto.SendMessageFrom;
import com.pdd.commons.response.ResponseEntity;
import com.pdd.message.api.feign.API_Blog_Message;
import com.pdd.message.server.service.Blog_MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author:liyangpeng
 * @date:2019/7/30 14:31
 */
@RestController
@RequestMapping("/blog_message")
public class Blog_MessageController implements API_Blog_Message {

    @Autowired
    private Blog_MessageService blog_messageService;

    /**
     * 查询TCP推送服务列表
     * @return
     */
    @Override
    public ResponseEntity getTcpServerList() {
        return blog_messageService.getTcpServerList();
    }

    /**
     * 发送私聊消息
     * @param sendMessageFrom
     * @return
     */
    @Override
    public ResponseEntity sendMessage(@RequestBody SendMessageFrom sendMessageFrom) {
        return blog_messageService.sendMessage(sendMessageFrom);
    }

    /**
     * 消息历史记录查询
     * @param pageIndex
     * @param pageSize
     * @param author_id
     * @return
     */
    @Override
    public ResponseEntity getMessageHistory(Integer pageIndex, Integer pageSize, Integer author_id) {
        return blog_messageService.getMessageHistory(pageIndex,pageSize,author_id);
    }

    /**
     * 查询消息列表
     * @return
     */
    @Override
    public ResponseEntity getMessageList() {
        return blog_messageService.getMessageList();
    }

    /**
     * 删除聊天窗口
     * @param jsonObject
     * @return
     */
    @Override
    public ResponseEntity delMessageList(JSONObject jsonObject) {
        return blog_messageService.delMessageList(jsonObject);
    }

    /**
     * 创建聊天窗口
     * @param jsonObject
     * @return
     */
    @Override
    public ResponseEntity createMessageBox(JSONObject jsonObject) {
        return blog_messageService.createMessageBox(jsonObject);
    }
}
