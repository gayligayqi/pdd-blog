package com.pdd.message.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pdd.commons.entity.dao.Blog_Message;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author:liyangpeng
 * @date:2019/8/9 14:09
 */
@Mapper
public interface Blog_MessageMapper extends BaseMapper<Blog_Message> {

    IPage<Blog_Message> selectPage(IPage<Blog_Message> page, @Param("msg_author_id") Integer msg_author_id, @Param("msg_author_id_s") Integer msg_author_id_s);
}
