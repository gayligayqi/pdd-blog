package com.pdd.message.server.service;

import com.alibaba.fastjson.JSONObject;
import com.pdd.commons.entity.dto.SendMessageFrom;
import com.pdd.commons.entity.vo.AccountInfo_Vo;
import com.pdd.commons.entity.vo.Message_Vo;
import com.pdd.commons.response.ResponseEntity;

/**
 * @author:liyangpeng
 * @date:2019/7/30 15:00
 */
public interface Blog_MessageService {
    /**
     * TCP链接列表
     * @return
     */
    ResponseEntity getTcpServerList();

    /**
     * 发送消息
     * @param sendMessageFrom
     * @return
     */
    ResponseEntity sendMessage(SendMessageFrom sendMessageFrom);

    /**
     * 生成系统提示未读消息
     * @return
     */
    Message_Vo sendUnreadMessage(AccountInfo_Vo accountInfo_vo);
    /**
     * 查询查询聊天记录
     */
    ResponseEntity getMessageHistory(Integer pageIndex,Integer pageSize,Integer author_id);

    /**
     * 查询消息列表
     * @return
     */
    ResponseEntity getMessageList();

    /**
     * 删除聊天窗口
     * @param jsonObject
     * @return
     */
    ResponseEntity delMessageList(JSONObject jsonObject);

    /**
     * 创建聊天窗口
     * @return
     */
    ResponseEntity createMessageBox(JSONObject jsonObject);
}
