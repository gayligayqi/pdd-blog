package com.pdd.message.server.service;

/**
 * @author:liyangpeng
 * @date:2019/7/30 10:13
 */
public interface SocketIOService {
    /**
     * 推送的事件
     */
    public final static String PUSH_EVENT = "push_event";
    /**
     * 接收时间
     */
    public final static String RECEIVE_EVENT="receive_event";

    /**
     * 启动服务
     * @throws Exception
     */
    void start() throws Exception;

    /**
     * 停止服务
     */
    void stop();
}
