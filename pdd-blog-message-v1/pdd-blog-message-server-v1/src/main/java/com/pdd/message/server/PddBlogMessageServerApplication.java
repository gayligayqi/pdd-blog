package com.pdd.message.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author:liyangpeng
 * @date:2019/7/30 10:19
 */
@SpringBootApplication(scanBasePackages = {"com.pdd.message.server","com.pdd.commons"},excludeName ="com.pdd.article.api")
@EnableEurekaClient
public class PddBlogMessageServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(PddBlogMessageServerApplication.class,args);
    }
}