package com.pdd.message.server.commons.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextClosedEvent;

/**
 * @author:liyangpeng
 * 程序安全退出链接【请勿暴力关闭】自主退出消息推送服务
 * @date:2019/7/30 14:11
 */
@Configuration
@Slf4j
public class ApplicationShutDownHook implements ApplicationListener<ContextClosedEvent> {

    @Autowired
    private SocketIoConfig socketIoConfig;

    @Override
    public void onApplicationEvent(ContextClosedEvent contextClosedEvent) {
        socketIoConfig.shuwDownServer();
    }
}
