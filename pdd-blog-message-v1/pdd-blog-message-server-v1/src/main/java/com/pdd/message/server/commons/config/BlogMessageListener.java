package com.pdd.message.server.commons.config;

import com.alibaba.fastjson.JSON;
import com.corundumstudio.socketio.SocketIOClient;
import com.pdd.commons.entity.vo.Message_Vo;
import com.pdd.message.server.service.SocketIOService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;

/**
 * @author:liyangpeng
 * @date:2019/8/1 11:44
 */
public class BlogMessageListener implements MessageListener {

    @Autowired
    private SocketIOClient socketIOClient;

    public BlogMessageListener(SocketIOClient socketIOClient){
        this.socketIOClient=socketIOClient;
    }

    @Override
    public void onMessage(Message message, byte[] bytes) {
        String JsonObject=new String(message.getBody());
        Message_Vo message_vo= JSON.parseObject(JsonObject,Message_Vo.class);
        socketIOClient.sendEvent(SocketIOService.PUSH_EVENT,message_vo);
    }
}
