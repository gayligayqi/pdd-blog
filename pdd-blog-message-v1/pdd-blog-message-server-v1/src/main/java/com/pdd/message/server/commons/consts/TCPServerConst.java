package com.pdd.message.server.commons.consts;

/**
 * @author:liyangpeng
 * @date:2019/7/30 14:17
 */
public class TCPServerConst {
    /**
     * PDD服务注册id
     */
    public static final String SERVER_ID="PDD-MESSAGE-TCP-SERVER-ID";
    /**
     * TCP链接后缀
     */
    public static final String CLIENT_SUFFIX="_push_tcp_queue";
    /**
     * 消息离线
     */
    public static final String CLIENT_OFFLINE_SUFFIX="_offline_tcp_queue";
}
