package com.pdd.feign;

import feign.Contract;
import feign.Logger;
import feign.RequestInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FeignConfiguration {

    @Autowired
    private FeignRequestInterceptor feignRequestInterceptor;
    /**
     * feign日志配置
     * @return
     */
    @Bean
    Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }

    /**
     * feign请求头传递
     * @return
     */
    @Bean
    public RequestInterceptor requestInterceptor(){
        return feignRequestInterceptor;
    }

    /**
     * 启用feign契约
     * @RequestLine("GET /TEST")
     * @return
     */
//    @Bean
//    public Contract feignContract() {
//        //这将SpringMvc Contract 替换为feign.Contract.Default
//        return new feign.Contract.Default();
//    }
}
