package com.pdd.feign;

import com.pdd.commons.utils.WebAreaUtil;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

/**
 * @author:liyangpeng
 * @date:2019/7/16 17:30
 */
@Component
public class FeignRequestInterceptor implements RequestInterceptor {

    @Autowired
    private WebAreaUtil webAreaUtil;

    @Override
    public void apply(RequestTemplate requestTemplate) {
        HttpServletRequest request=webAreaUtil.getRequest();
        Enumeration<String> headerNames = request.getHeaderNames();
        if (headerNames != null) {
            while (headerNames.hasMoreElements()) {
                String name = headerNames.nextElement();
                String values = request.getHeader(name);
                requestTemplate.header(name, values);
            }
        }
    }
}
