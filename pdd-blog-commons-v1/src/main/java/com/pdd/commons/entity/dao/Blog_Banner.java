package com.pdd.commons.entity.dao;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.util.Date;

/**
 * @author:liyangpeng
 * @date:2019/7/9 19:51
 */
@Data
@TableName("blog_banner")
public class Blog_Banner extends Model<Blog_Banner> {
    /**
     * 自增id
     */
    @JSONField(serialize = false)
    @TableId(value="id",type = IdType.AUTO)
    private Integer id;
    /**
     * 标题
     */
    @TableField("title")
    private String title;
    /**
     * banner图
     */
    @TableField("bannerPic")
    private String bannerPic;
    /**
     * 文章id
     */
    @TableField("contentid")
    private Integer contentid;
    /**
     * 添加时间
     */
    @TableField("addtime")
    @JSONField(serialize = false)
    private Date addtime;
    /**
     * 修改时间
     */
    @TableField("modifyTime")
    @JSONField(serialize = false)
    private Date modifyTime;
    /**
     * 添加用户
     */
    @TableField("adduser")
    @JSONField(serialize = false)
    private String adduser;
    /**
     * 是否删除 0:否 1:是
     */
    @TableField("isdel")
    @JSONField(serialize = false)
    private Integer isdel;
}
