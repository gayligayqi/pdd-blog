package com.pdd.commons.entity.vo;

import lombok.Data;

import java.util.Date;

/**
 * @author:liyangpeng
 * @date:2019/6/27 20:22
 */
@Data
public class UserIndex_Vo {

    private Integer authorId;

    private String nickName;

    private Integer level;

    private Integer bigV;

    private Integer exp;

    private String avatar;

    private String perSign;

    private Integer article;

    private Integer comment;

    private Date lastActiveTime;

    private Integer isFan;

    private String backImg;

    private Integer nextLevelExp;

}
