package com.pdd.commons.entity.dao;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @author:liyangpeng
 * @date:2019/7/16 9:35
 */
@Data
@TableName("blog_zone_access")
public class Blog_Zone_Access {

    @TableId(value="id",type = IdType.AUTO)
    private Integer id;
    /**
     * 访问者
     */
    @TableField("authorid")
    private Integer authorid;
    /**
     * 被访问者
     */
    @TableField("authorid_s")
    private Integer authorid_s;
    /**
     * 访问时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @TableField("accesstime")
    private Date accesstime;
    /**
     * 流浪器
     */
    @TableField("browser")
    private String browser;
    /**
     * 系统版本
     */
    @TableField("osName")
    private String osName;
    /**
     * 访问地址
     */
    @TableField("location")
    private String location;
    /**
     * 平台
     */
    @TableField("platform")
    private String platform;

    /*********************附加字段***********************/
    @TableField(exist =false)
    private Blog_Account accessUser;
}
