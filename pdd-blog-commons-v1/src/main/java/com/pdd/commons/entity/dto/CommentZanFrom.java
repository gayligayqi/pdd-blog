package com.pdd.commons.entity.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author:liyangpeng
 * @date:2019/7/5 10:52
 */
@Data
public class CommentZanFrom {
    /**
     * 评论id
     */
    @NotNull(message = "评论id不能为空")
    private Integer commentId;
    /**
     * 点赞状态
     */
    @NotNull(message = "点赞状态不能为空")
    private Integer clickStatus;
}
