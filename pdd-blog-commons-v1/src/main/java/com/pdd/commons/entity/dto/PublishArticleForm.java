package com.pdd.commons.entity.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author:liyangpeng
 * @date:2019/6/19 11:13
 */
@Data
public class PublishArticleForm {
    /**
     * 标题
     */
    @NotNull(message = "标题不能为空")
    @NotEmpty(message = "标题不能为空")
    private String title;
    /**
     * 封面
     */
    @NotNull(message = "请上传封面")
    @NotEmpty(message = "请上传封面")
    private String cover;
    /**
     * 关键字
     */
    @NotNull(message = "关键字不能为空")
    @NotEmpty(message = "关键字不能为空")
    private String keyWords;
    /**
     * 文章分类
     */
    @NotNull(message = "文章分类不能为空")
    @NotEmpty(message = "文章分类不能为空")
    private String artType;
    /**
     * 发布类型
     */
    @NotNull(message = "发布类型不能为空")
    @NotEmpty(message = "发布类型不能为空")
    private String publisType;
    /**
     * 是否添加水印
     */
    private Boolean watemark;
    /**
     * 是否开启评论
     */
    private Boolean canComment;
    /**
     * 文章内容
     */
    @NotNull(message = "发布类型不能为空")
    @NotEmpty(message = "发布类型不能为空")
    private String content;
}