package com.pdd.commons.entity.dao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.util.Date;

/**
 * @author:liyangpeng
 * @date:2019/7/9 10:31
 */
@Data
@TableName("blog_article_collection")
public class Blog_Article_Collection extends Model<Blog_Article_Collection> {
    /**
     * 自增id
     */
    @TableId(value="id",type = IdType.AUTO)
    private Integer id;
    /**
     * 文章id
     */
    @TableField("articleId")
    private Integer articleId;
    /**
     * 作者id
     */
    @TableField("author_id")
    private Integer author_id;
    /**
     * 收藏状态 0:未收藏 1:已收藏
     */
    @TableField("collStatus")
    private Integer collStatus;
    /**
     * 收藏时间
     */
    @TableField("clickTime")
    private Date clickTime;
    /**
     * 修改时间
     */
    @TableField("modifyTime")
    private Date modifyTime;
}
