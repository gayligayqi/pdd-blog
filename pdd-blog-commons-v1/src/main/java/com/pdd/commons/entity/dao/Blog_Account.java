package com.pdd.commons.entity.dao;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.util.Date;

/**
 * @author:liyangpeng
 * @date:2019/6/21 15:30
 */
@Data
@TableName("blog_account")
public class Blog_Account extends Model<Blog_Account> {
    /**
     * 用户id
     */
    @TableId(value="id",type = IdType.AUTO)
    private Integer id;
    /**
     * 用户名
     */
    @TableField("account")
    private String account;
    /**
     * 密码
     */
    @JSONField(serialize = false)
    @TableField("password")
    private String password;
    /**
     * 昵称
     */
    @TableField("nickName")
    private String nickName;
    /**
     * 邮箱
     */
    @TableField("email")
    private String email;
    /**
     * 其他联系方式
     */
    @TableField("otherContact")
    private String otherContact;
    /**
     * 性别
     */
    @TableField("sex")
    private Integer sex;
    /**
     * 注册Ip
     */
    @JSONField(serialize = false)
    @TableField("registIp")
    private String registIp;
    /**
     * 注册设备
     */
    @JSONField(serialize = false)
    @TableField("registDevice")
    private String registDevice;
    /**
     * 个性签名
     */
    @TableField("perSign")
    private String perSign;
    /**
     * 是否锁定
     */
    @TableField("islock")
    private Integer islock;
    /**
     * 注册时间
     */
    @TableField("registTime")
    private Date registTime;
    /**
     * 头像
     */
    @TableField("avatar")
    private String avatar;
    /**
     * 大V
     */
    @TableField("bigV")
    private Integer bigV;
    /**
     * 是否可发布文章
     */
    @TableField("canPublish")
    private Integer canPublish;
    /**
     * 背景图
     */
    @TableField("backImg")
    private String backImg;
    /**
     * 等级
     */
    @TableField("level")
    private Integer level;

    /************************附加字段***************************/
    @TableField(exist = false)
    private String session_id;
}
