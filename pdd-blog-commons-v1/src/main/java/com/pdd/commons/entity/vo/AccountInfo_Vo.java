package com.pdd.commons.entity.vo;

import lombok.Data;

import java.util.Date;

/**
 * @author:liyangpeng
 * @date:2019/6/21 17:51
 */
@Data
public class AccountInfo_Vo {
    /**
     * 用户id
     */
    private Integer id;
    /**
     * 用户名
     */
    private String account;
    /**
     * 昵称
     */
    private String nickName;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 其他联系方式
     */
    private String otherContact;
    /**
     * 性别
     */
    private Integer sex;
    /**
     * 个性签名
     */
    private String perSign;
    /**
     * 是否锁定
     */
    private Integer islock;
    /**
     * 注册时间
     */
    private Date registTime;
    /**
     * 头像
     */
    private String avatar;
    /**
     * 大V
     */
    private Integer bigV;
    /**
     * 是否可发布文章
     */
    private Integer canPublish;
    /**
     * 背景图
     */
    private String backImg;
}
