package com.pdd.commons.entity.vo;

import lombok.Data;

/**
 * @author:liyangpeng
 * @date:2019/6/20 14:14
 */
@Data
public class Blog_Image_Vo {
    /**
     * 图片md5
     */
    private String md5;
    /**
     * 访问链接
     */
    private String src;
    /**
     * 高
     */
    private Integer heigth;
    /**
     * 宽
     */
    private Integer width;
    /**
     * 文件大小
     */
    private long size;
    /**
     * 图片悬浮提示
     */
    private String alt;
    /**
     * 水印图
     */
    private String wmk;
}
