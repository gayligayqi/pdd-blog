package com.pdd.commons.entity.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author:liyangpeng
 * @date:2019/7/3 17:18
 */
@Data
public class RegisterForm {
    /**
     * 账号
     */
    @NotNull
    @NotEmpty
    private String account;
    /**
     * 昵称
     */
    @NotNull
    @NotEmpty
    private String nickName;
    /**
     * 密码
     */
    @NotNull
    @NotEmpty
    private String password;
    /**
     * 性别
     */
    @NotNull
    private Integer sex;
    /**
     * 邮箱
     */
    @NotNull
    @NotEmpty
    private String email;
    /**
     * 验证码
     */
    @NotNull
    @NotEmpty
    private String code;
}
