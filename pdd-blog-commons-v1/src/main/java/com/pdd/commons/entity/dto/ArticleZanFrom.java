package com.pdd.commons.entity.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author:liyangpeng
 * @date:2019/7/9 9:50
 */
@Data
public class ArticleZanFrom {
    /**
     * 内容id
     */
    @NotNull(message = "内容id不能为空")
    private Integer contentId;
    /**
     * 点赞状态
     */
    @NotNull(message = "点赞状态不能为空")
    private Integer clickStatus;
}
