package com.pdd.commons.entity.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author:liyangpeng
 * @date:2019/7/5 15:46
 */
@Data
public class ReplyCommentForm {

    @NotNull(message = "回复内容不能为空")
    private String replyText;

    @NotNull(message = "被回复的用户不能为空")
    private Integer reply_author_id_s;

    @NotNull(message = "被回复的评论不能为空")
    private Integer commentId;


}
