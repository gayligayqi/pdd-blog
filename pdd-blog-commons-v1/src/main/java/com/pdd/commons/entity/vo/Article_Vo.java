package com.pdd.commons.entity.vo;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @author:liyangpeng
 * @date:2019/6/20 14:10
 */
@Data
public class Article_Vo {
    /**
     * 缓存key
     */
    private String cacheKey;
    /**
     * 文章内容
     */
    private String content;
    /**
     * 文章标题
     */
    private String title;
    /**
     * 文章描述
     */
    private String artDesc;
    /**
     * 发布时间
     */
    private Date publishTime;
    /**
     * 文章分类
     */
    private String artType;
    /**
     * 发布类型
     */
    private String publisType;
    /**
     * 正文原图
     */
    private List<Blog_Image_Vo> imgjs;
    /**
     * 作者id
     */
    private Object author_id;
    /**
     * 封面
     */
    private String cover;
    /**
     * 关键字
     */
    private String keyWords;
}
