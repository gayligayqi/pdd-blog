package com.pdd.commons.entity.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author:liyangpeng
 * @date:2019/7/4 20:12
 */
@Data
public class AddCommentFrom {

    @NotNull(message = "评论不能为空")
    @NotEmpty(message = "评论不能为空")
    private String content;

    @NotNull(message = "文章id不能为空")
    private Integer contentId;
}
