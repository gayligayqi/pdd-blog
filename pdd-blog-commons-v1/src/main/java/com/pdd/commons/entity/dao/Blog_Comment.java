package com.pdd.commons.entity.dao;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @author:liyangpeng
 * @date:2019/7/4 19:59
 */
@Data
@TableName("blog_comment")
public class Blog_Comment extends Model<Blog_Comment> {
    /**
     * 评论id
     */
    @TableId(value="id",type = IdType.AUTO)
    private Integer id;
    /**
     * 作者id
     */
    @TableField("author_id")
    private Integer author_id;
    /**
     * 评论内容
     */
    @TableField("content")
    private String content;
    /**
     * 发布时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @TableField("publishTime")
    private Date publishTime;
    /**
     * 文章id
     */
    @TableField("articleId")
    private Integer articleId;
    /**
     * 浏览器
     */
    @TableField("browser")
    private String browser;
    /**
     * 系统版本
     */
    @TableField("osName")
    private String osName;
    /**
     * ip
     */
    @TableField("ip")
    private String ip;
    /**
     * 位置
     */
    @TableField("location")
    private String location;
    /**
     * 平台标识
     */
    @TableField("platform")
    private String platform;
    /**
     * 是否删除 0:否 1:是
     */
    @TableField("isdel")
    @JSONField(serialize = false)
    private Integer isdel;

    /**************************附加字段************************/
    @TableField(exist = false)
    private Blog_Account user;
    /**
     * 点赞状态
     */
    @TableField("clickStatus")
    private Integer clickStatus;
    /**
     * 回复
     */
    @TableField(exist = false)
    private List<Blog_Comment_Reply> Reply;
    /**
     * 文章
     */
    @TableField(exist = false)
    private Blog_Article blog_article;
}
