package com.pdd.commons.entity.vo;

import lombok.*;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author:liyangpeng
 * @date:2019/7/30 10:14
 */
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Message_Vo {

    private Message_User_Vo user_vo;

    private Integer author_id;

    private String content;

    private int self;

    private Date timeStamp;
}
