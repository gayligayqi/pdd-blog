package com.pdd.commons.entity.dao;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.pdd.commons.entity.vo.Message_User_Vo;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author:liyangpeng
 * @date:2019/8/8 20:39
 */
@Data
@Accessors(chain = true)
@TableName("blog_message")
public class Blog_Message extends Model<Blog_Message> {
    /**
     * 消息id
     */
    @TableId(value="id",type = IdType.AUTO)
    private Integer id;
    /**
     * 发送者
     */
    @TableField("msg_author_id")
    private Integer msg_author_id;
    /**
     * 接收者
     */
    @TableField("msg_author_id_s")
    private Integer msg_author_id_s;
    /**
     * 消息类型
     */
    @TableField("msg_type")
    private Integer msg_type;
    /**
     * 消息内容
     */
    @TableField("content")
    private String content;
    /**
     * 发布时间
     */
    @TableField("publishTime")
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date publishTime;
    /**
     * 附加用户字段
     */
    @TableField(exist = false)
    private Message_User_Vo user_vo;
}
