package com.pdd.commons.entity.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author:liyangpeng
 * @date:2019/7/26 11:18
 */
@Data
public class ModifyUserInfoForm {

    @NotNull(message = "昵称不能为空")
    @NotEmpty(message = "昵称不能为空")
    private String nickName;

    @NotNull(message = "性别不能为空")
    private Integer sex;

    private String perSign;

    @NotNull(message = "头像头像不能为空")
    @NotEmpty(message = "头像不能为空")
    private String avatar;

    @NotNull(message = "背景图不能为空")
    @NotEmpty(message = "背景图不能为空")
    private String backImg;
}
