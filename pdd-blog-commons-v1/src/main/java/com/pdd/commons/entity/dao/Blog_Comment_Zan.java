package com.pdd.commons.entity.dao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

/**
 * @author:liyangpeng
 * @date:2019/7/5 10:34
 */
@Data
@TableName("blog_comment_zan")
public class Blog_Comment_Zan extends Model<Blog_Comment_Zan> {
    /**
     * 自增id
     */
    @TableId(value="id",type = IdType.AUTO)
    private Integer id;
    /**
     * 评论id
     */
    @TableField("commentId")
    private Integer commentId;
    /**
     * 用户id
     */
    @TableField("author_id")
    private Integer author_id;
    /**
     * 点赞状态 0: 未赞  1:已赞
     */
    @TableField("clickStatus")
    private Integer clickStatus;
}
