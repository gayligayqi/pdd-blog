package com.pdd.commons.entity.dao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.util.Date;

/**
 * @author:liyangpeng
 * @date:2019/6/27 19:57
 */
@TableName("blog_login_log")
@Data
public class Blog_Login_Log extends Model<Blog_Login_Log> {
    /**
     * 自增id
     */
    @TableId(value="id",type = IdType.AUTO)
    private Integer id;
    /**
     * 用户id
     */
    @TableField("author_id")
    private Integer author_id;
    /**
     * 设备
     */
    @TableField("version")
    private String version;
    /**
     * 平台
     */
    @TableField("platform")
    private String platform;
    /**
     * IP
     */
    @TableField("ip")
    private String ip;
    /**
     * 添加时间
     */
    @TableField("addtime")
    private Date addtime;
    /**
     * 系统
     */
    @TableField("osname")
    private String osname;
    /**
     * 浏览器跟版本号
     */
    @TableField("browser")
    private String browser;
}
