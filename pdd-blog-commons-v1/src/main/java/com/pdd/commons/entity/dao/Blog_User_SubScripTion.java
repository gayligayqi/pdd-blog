package com.pdd.commons.entity.dao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.util.Date;

/**
 * @author:liyangpeng
 * @date:2019/8/13 9:54
 */
@Data
@TableName("blog_user_subscription")
public class Blog_User_SubScripTion extends Model<Blog_User_SubScripTion> {
    /**
     * 自增id
     */
    @TableId(value="id",type = IdType.AUTO)
    private Integer id;
    /**
     *发起订阅人
     */
    @TableField("author_id")
    private Integer author_id;
    /**
     *被订阅人
     */
    @TableField("care_author_id")
    private Integer care_author_id;
    /**
     *创建时间
     */
    @TableField("addtime")
    private Date addtime;
    /**
     * 0 : 未取消订阅 1：订阅
     */
    @TableField("issubc")
    private Integer issubc;
    /**
     * 修改时间
     */
    @TableField("modifyTime")
    private Date modifyTime;
}
