package com.pdd.commons.entity.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author:liyangpeng
 * @date:2019/7/30 20:11
 */
@Data
public class SendMessageFrom {

    @NotNull(message = "author_id不能为空")
    private Integer author_id;

    @NotNull(message = "content不能为空")
    @NotEmpty(message = "content不能为空")
    private String content;

    @NotNull(message = "msg_type不能为空")
    private Integer msg_type;
}
