package com.pdd.commons.entity.dao;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.util.Date;

/**
 * @author:liyangpeng
 * @date:2019/6/21 10:08
 */
@Data
@TableName("blog_article_type")
public class Blog_Article_Type extends Model<Blog_Article_Type> {
    /**
     * 自增id
     */
    @TableId(value="id",type = IdType.AUTO)
    private int id;
    /**
     * 文章分类
     */
    @TableField("artType")
    private String artType;
    /**
     * 是否禁用 0 否 1是
     */
    @TableField("disable")
    private Integer disable;
    /**
     * 是否删除 0 否 1是
     */
    @TableField("isdel")
    private Integer isdel;
    /**
     * 添加时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @TableField("addtime")
    private Date addtime;
}
