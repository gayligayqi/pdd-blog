package com.pdd.commons.entity.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author:liyangpeng
 * @date:2019/7/30 20:21
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Message_User_Vo {

    private Integer id;

    private String nickName;

    private String avatar;

    private Integer bigV;
}
