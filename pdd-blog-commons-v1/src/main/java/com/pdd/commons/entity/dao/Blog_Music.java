package com.pdd.commons.entity.dao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

/**
 * @author:liyangpeng
 * @date:2019/8/20 14:28
 */
@Data
@TableName("blog_music")
public class Blog_Music extends Model<Blog_Music> {
    /**
     * 自增id
     */
    @TableId(value="id",type = IdType.AUTO)
    private Integer id;
    /**
     * 歌曲id
     */
    @TableField("songId")
    private Integer songId;
    /**
     * 歌曲封面
     */
    @TableField("songCover")
    private String songCover;
    /**
     * 歌曲名称
     */
    @TableField("songName")
    private String songName;
    /**
     * 歌手
     */
    @TableField("singer")
    private String singer;
    /**
     * 播放地址
     */
    @TableField("url")
    private String url;
    /**
     * 时长
     */
    @TableField("duration")
    private Integer duration;
    /**
     * url来源
     */
    @TableField("urlfrom")
    private String urlfrom;
    /**
     * 是否删除 0:否 1:是
     */
    @TableField("isdel")
    private Integer isdel;
    /**
     * 歌词
     */
    @TableField("lyrics")

    private String lyrics;
}
