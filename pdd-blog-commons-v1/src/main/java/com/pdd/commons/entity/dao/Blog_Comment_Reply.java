package com.pdd.commons.entity.dao;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.util.Date;

/**
 * @author:liyangpeng
 * @date:2019/7/5 15:35
 */
@Data
@TableName("blog_comment_reply")
public class Blog_Comment_Reply extends Model<Blog_Comment_Reply> {
    /**
     * 自增id
     */
    @TableId(value="id",type = IdType.AUTO)
    private Integer id;
    /**
     * 评论id
     */
    @TableField("commentId")
    private Integer commentId;
    /**
     * 评论内容
     */
    @TableField("replyText")
    private String replyText;
    /**
     * 回复时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @TableField("replyTime")
    private Date replyTime;
    /**
     * 回复用户
     */
    @TableField("reply_author_id")
    private Integer reply_author_id;
    /**
     * 被回复的用户
     */
    @TableField("reply_author_id_s")
    private Integer reply_author_id_s;
    /**
     * 平台
     */
    @TableField("platform")
    private String platform;
    /**
     * 位置
     */
    @TableField("location")
    private String location;
    /**
     * 系统版本
     */
    @TableField("osName")
    private String osName;
    /**
     * 浏览器
     */
    @TableField("browser")
    private String browser;
    /**
     * 发布人ip
     */
    @TableField("ip")
    private String ip;
    /**
     * 删除 0：否 1：是
     */
    @TableField("isdel")
    @JSONField(serialize = false)
    private Integer isdel;

    /************************附加字段**************************/
    /**
     * 被@的人
     */
    @TableField(exist = false)
    private Blog_Account reply_user;
    /**
     * 回复用户
     */
    @TableField(exist = false)
    private Blog_Account user;
}
