package com.pdd.commons.entity.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author:liyangpeng
 * @date:2019/7/9 10:38
 */
@Data
public class ArticleCollectionFrom {

    @NotNull(message = "内容id不能为空")
    private Integer contentId;

    @NotNull(message = "收藏状态不能为空")
    private Integer collStatus;
}
