package com.pdd.commons.entity.dao;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.util.Date;

/**
 * @author:liyangpeng
 * @date:2019/6/19 11:13
 */
@Data
@TableName("blog_article")
public class Blog_Article extends Model<Blog_Article> {
    /**
     * 文章id
     */
    @TableId(value="id",type = IdType.AUTO)
    private Integer id;
    /**
     * 标题
     */
    @TableField("title")
    private String title;
    /**
     * 封面
     */
    @TableField("cover")
    private String cover;
    /**
     * 作者
     */
    @TableField("author_id")
    private Integer author_id;
    /**
     * 关键字
     */
    @TableField("keyWords")
    private String keyWords;
    /**
     * 文章分类
     */
    @TableField("artType")
    private String artType;
    /**
     * 发布类型
     */
    @TableField("publisType")
    private String publisType;
    /**
     * 文章内容缓存key
     */
    @TableField("cacheKey")
    private String cacheKey;
    /**
     * 文章标签
     */
    @TableField("artTag")
    private String artTag;
    /**
     * 发布时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @TableField("publishTime")
    private Date publishTime;
    /**
     * 文章描述
     */
    @TableField("artDesc")
    private String artDesc;
    /**
     * 是否可评论 false：否 1true：是
     */
    @TableField("canComment")
    private Boolean canComment;
    /**
     * 管理员置顶
     */
    @TableField("adminToping")
    private Integer adminToping;
    /**
     * 用户置顶
     */
    @TableField("userToping")
    private Integer userToping;
    /**
     * 审核标记
     */
    @JSONField(serialize = false)
    @TableField("ispass")
    private Integer ispass;
    /**
     * 是否删除
     */
    @JSONField(serialize = false)
    @TableField("isdel")
    private Integer isdel;

    /*******************************附加内容字段*********************************/
    /**
     * 用户
     */
    @TableField(exist=false)
    private Blog_Account user;
    /**
     * html富文本标签
     */
    @TableField(exist=false)
    private String content;
    /**
     * 是否收藏
     */
    @TableField(exist=false)
    private Integer isColl=0;
    /**
     * 是否赞
     */
    @TableField(exist=false)
    private Integer isZan=0;
}
