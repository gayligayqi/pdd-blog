package com.pdd.commons.entity.dao;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.util.Date;

/**
 * @author:liyangpeng
 * @date:2019/7/10 9:22
 */
@Data
@TableName("blog_update_history")
public class Blog_Update_History extends Model<Blog_Update_History> {

    /**
     * 自增id
     */
    @TableId(value="id",type = IdType.AUTO)
    private Integer id;
    /**
     * 更新内容
     */
    @TableField("content")
    private String content;
    /**
     * 版本号
     */
    @TableField("version")
    private String version;
    /**
     * 发布时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @TableField("publishTime")
    private Date publishTime;
    /**
     * 发布人
     */
    @TableField("addUser")
    private String addUser;
}
