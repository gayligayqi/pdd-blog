package com.pdd.commons.entity.dao;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.util.Date;

/**
 * @author:liyangpeng
 * @date:2019/7/16 14:01
 */
@Data
@TableName("blog_account_achievement_log")
public class Blog_Account_AchieveMent_Log extends Model<Blog_Account_AchieveMent_Log> {
    /**
     * 自增id
     */
    @TableId(value="id",type = IdType.AUTO)
    private Integer id;
    /**
     * 作者id
     */
    @TableField("author_id")
    private Integer author_id;
    /**
     * 获取成就类型
     */
    @TableField("type")
    private Integer type;
    /**
     *内容
     */
    @TableField("content")
    private String content;
    /**
     * 添加时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @TableField("addtime")
    private Date addtime;
    /**
     * 是否删除
     */
    @TableField("isdel")
    private Integer isdel;
}
