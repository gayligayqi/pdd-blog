package com.pdd.commons.entity.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author:liyangpeng
 * @date:2019/6/21 16:18
 */
@Data
public class LoginForm {

    @NotEmpty(message = "用户名不能为空")
    @NotNull(message = "用户名不能为空")
    private String account;

    @NotEmpty(message = "密码不能为空")
    @NotNull(message = "密码不能为空")
    private String password;

    private Boolean remember;

    @NotEmpty(message = "平台唯一标识不能为空")
    @NotNull(message = "平台唯一标识不能为空")
    private String platform;
}
