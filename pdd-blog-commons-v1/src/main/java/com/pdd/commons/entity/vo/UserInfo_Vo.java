package com.pdd.commons.entity.vo;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.util.Date;

/**
 * @author:liyangpeng
 * @date:2019/7/26 10:32
 */
@Data
public class UserInfo_Vo {
    /**
     * 用户id
     */
    private Integer id;
    /**
     * 用户名
     */
    private String account;
    /**
     * 昵称
     */
    private String nickName;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 其他联系方式
     */
    private String otherContact;
    /**
     * 性别
     */
    private Integer sex;
    /**
     * 注册Ip
     */
    private String registIp;
    /**
     * 注册设备
     */
    private String registDevice;
    /**
     * 个性签名
     */
    private String perSign;
    /**
     * 注册时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private Date registTime;
    /**
     * 头像
     */
    private String avatar;
    /**
     * 大V
     */
    private Integer bigV;
    /**
     * 背景图
     */
    private String backImg;
    /**
     * 等级
     */
    private Integer level;

}
