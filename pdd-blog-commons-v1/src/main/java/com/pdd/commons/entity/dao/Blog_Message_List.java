package com.pdd.commons.entity.dao;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.pdd.commons.entity.vo.Message_User_Vo;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author:liyangpeng
 * @date:2019/8/12 11:07
 */
@Data
@TableName("blog_message_list")
@Accessors(chain = true)
public class Blog_Message_List extends Model<Blog_Message_List> {
    /**
     * 自增id
     */
    @TableId(value="id",type = IdType.AUTO)
    private Integer id;
    /**
     *自己
     */
    @TableField("msg_author_id")
    private Integer msg_author_id;
    /**
     *对方
     */
    @TableField("msg_author_id_s")
    private Integer msg_author_id_s;
    /**
     * 未读
     */
    @TableField("unread")
    private Integer unread;
    /**
     * 创建时间
     */
    @TableField("createtime")
    private Date createtime;
    /**
     * 最后更新时间
     */
    @TableField("lastTime")
    private Date lastTime;
    /**
     * 是否删除 0:不删除 1:删除
     */
    @TableField("isdel")
    private Integer isdel;

    /******************************附加字段************************************/
    /**
     * 对方用户信息
     */
    @TableField(exist = false)
    private Message_User_Vo user_vo;
}
