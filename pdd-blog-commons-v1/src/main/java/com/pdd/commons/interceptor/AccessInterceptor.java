package com.pdd.commons.interceptor;

import com.pdd.commons.utils.UserAgentUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author:liyangpeng
 * @date:2019/6/21 17:49
 */
@Component
@Slf4j
public class AccessInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.info("【pdd博客】用户:[{}]访问平台", UserAgentUtils.getOs(request));
        return true;
    }
}
