package com.pdd.commons.utils;

import com.alibaba.fastjson.JSON;
import com.pdd.commons.entity.vo.AccountInfo_Vo;
import com.pdd.commons.response.ResponseEntity;
import com.pdd.commons.response.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;


/**
 * @author:liyangpeng
 * @date:2019/6/21 17:50
 */
@Component
public class AccountUtil {

    @Autowired
    private RedisUtils redisUtils;

    private static final double SALT=20;

    /**
     * 获取登录信息
     * @return
     */
    public AccountInfo_Vo getLoginInfo(){
        //得到请求工具类
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request=servletRequestAttributes.getRequest();
        if(request==null){
            return null;
        }
        Cookie [] cookies=request.getCookies();
        if(cookies==null){
            return null;
        }
        //遍历cookie
        Cookie session=null;
        for (Cookie cookie:cookies){
            if(cookie.getName().equals(PublicKey.SESSION_KEY)){
                session=cookie;
            }
        }
        if(session==null){
            return null;
        }
        return getUserInfoByToken(session.getValue());
    }

    /**
     * 根据Token获取登录信息
     * @param token
     * @return
     */
    public AccountInfo_Vo getUserInfoByToken(String token){
        //redis获取登录信息
        String userInfo=redisUtils.stringGetStringByKey(token);
        if(StringUtils.isEmpty(userInfo)){
            return null;
        }
        //得到登录信息
        AccountInfo_Vo accountInfo_vo= JSON.parseObject(userInfo,AccountInfo_Vo.class);
        return accountInfo_vo;
    }
    /**
     * 计算下一级需要的经验
     * @param level
     * @return
     */
    public Integer getlevelExp(Integer level){
        return (int)(level*(level/SALT)*100);
    }

    /**
     * 退出登录
     * @return
     */
    public ResponseEntity loginOut(){
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request=servletRequestAttributes.getRequest();
        if(request==null){
            return null;
        }
        Cookie [] cookies=request.getCookies();
        if(cookies==null){
            return null;
        }
        //遍历cookie
        Cookie session=null;
        for (Cookie cookie:cookies){
            if(cookie.getName().equals(PublicKey.SESSION_KEY)){
                session=cookie;
            }
        }
        if(session==null){
            return null;
        }
        redisUtils.deleteFromRedis(session.getValue());
        return ResponseResult.RETURN_SUCCESS("退出成功");
    }
}
