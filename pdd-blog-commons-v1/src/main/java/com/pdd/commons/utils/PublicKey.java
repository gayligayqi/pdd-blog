package com.pdd.commons.utils;

/**
 * @author:liyangpeng
 * @date:2019/6/20 17:29
 */
public class PublicKey {
    /**
     * redis 文章数据key
     */
    public static final String ARTICLE_DATA_MAP="article_data_map";
    /**
     *密码加密盐值
     */
    public static final String PWD_SALTVALUE="dsadkalk!sa2$^$56das1";
    /**
     * 登录信息cookie
     */
    public static final String SESSION_KEY="session_id";
    /**
     * 注册验证码邮箱验证
     */
    public static final String REGISTER_EMAIL_PREFIX="register_email_";
}
