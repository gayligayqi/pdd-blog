package com.pdd.commons.response;

/**
 * @author:liyangpeng
 * @date:2018/12/7 15:01
 */
public class ResponseResult {

     public static ResponseEntity RETURN_SUCCESS(String message){
        ResponseEntity entity=new ResponseEntity();
        entity.setCode("0000");
        entity.setMessage(message);
        entity.setData(null);
        entity.setSuccess(true);
        return entity;
    }

    public static ResponseEntity RETURN_ERROR(String message){
        ResponseEntity entity=new ResponseEntity();
        entity.setCode("0001");
        entity.setMessage(message);
        entity.setData(null);
        entity.setSuccess(false);
        return entity;
    }
}
