package com.pdd.article.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author:liyangpeng
 * @date:2019/7/11 11:13
 */
@SpringBootApplication(scanBasePackages ={"com.pdd.article.api","com.pdd.commons","com.pdd.feign"},exclude = DataSourceAutoConfiguration.class)
@EnableEurekaClient
@EnableFeignClients
public class PddBlogArticleApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(PddBlogArticleApiApplication.class,args);
    }
}
