package com.pdd.article.api.fallback;

import com.pdd.article.api.feign.API_Blog_Comment_Zan;
import com.pdd.article.enums.WebResultEmun;
import com.pdd.article.response.ResponseResult;
import com.pdd.commons.entity.dto.CommentZanFrom;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.validation.Valid;

/**
 * @author:liyangpeng
 * @date:2019/7/5 10:54
 */
@Component
public class Blog_Comment_ZanFallback implements API_Blog_Comment_Zan {
    @Override
    public ResponseEntity clickZan(@Valid CommentZanFrom clickZanFrom) {
        return ResponseResult.Return(WebResultEmun.SERVER_INVOKE_COMMENT_ZAN_ERROR);
    }
}
