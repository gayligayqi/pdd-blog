package com.pdd.article.api.feign;

import com.pdd.article.api.fallback.Blog_Image_UploadFallback;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author:liyangpeng
 * @date:2019/6/19 11:55
 */
@FeignClient(value = "pdd-blog-article-server-v1", path = "image_upload",fallback = Blog_Image_UploadFallback.class)
public interface API_Blog_Image_Upload {

    @PostMapping(value = "/uploadImage",consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    ResponseEntity uploadImage(@RequestPart("file") MultipartFile file, @RequestParam("type")  Integer type);
}
