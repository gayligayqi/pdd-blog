package com.pdd.article.api.feign;

import com.alibaba.fastjson.JSONObject;
import com.pdd.article.api.fallback.Blog_CommentFallback;
import com.pdd.commons.entity.dto.AddCommentFrom;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;

/**
 * @author:liyangpeng
 * @date:2019/7/4 20:10
 */
@FeignClient(value = "pdd-blog-article-server-v1", path = "blog_comment", fallback = Blog_CommentFallback.class)
public interface API_Blog_Comment {

    /**
     * 发布评论的接口
     * @param addCommentFrom
     * @return
     */
    @PostMapping("/addComment")
    ResponseEntity addComment(@RequestBody AddCommentFrom addCommentFrom);

    /**
     * 用户评论管理查询评论
     * @return
     */
    @GetMapping("/getComment")
    ResponseEntity getComment(@RequestParam("pageIndex") Integer pageIndex, @RequestParam("pageSize") Integer pageSize);

    /**
     * 删除评论
     * @param jsonObject
     * @return
     */
    @PostMapping("/delComment")
    ResponseEntity delComment(@RequestBody JSONObject jsonObject);

}
