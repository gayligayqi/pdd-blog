package com.pdd.article.api.fallback;

import com.alibaba.fastjson.JSONObject;
import com.pdd.article.api.feign.API_Blog_Comment;
import com.pdd.article.enums.WebResultEmun;
import com.pdd.article.response.ResponseResult;
import com.pdd.commons.entity.dto.AddCommentFrom;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.validation.Valid;

/**
 * @author:liyangpeng
 * @date:2019/7/4 20:10
 */
@Component
public class Blog_CommentFallback implements API_Blog_Comment {

    @Override
    public ResponseEntity addComment(@Valid AddCommentFrom addCommentFrom) {
        return ResponseResult.Return(WebResultEmun.SERVER_INVOKE_ADD_COMMENT_ERROR);
    }

    @Override
    public ResponseEntity getComment(Integer pageIndex, Integer pageSize) {
        return ResponseResult.Return(WebResultEmun.SERVER_INVOKE_GETCOMMENT_ERROR);
    }

    @Override
    public ResponseEntity delComment(JSONObject jsonObject) {
        return ResponseResult.Return(WebResultEmun.SERVER_INVOKE_DELETE_COMMENT_ERROR);
    }
}
