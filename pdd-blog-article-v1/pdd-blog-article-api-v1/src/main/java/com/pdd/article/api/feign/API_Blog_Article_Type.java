package com.pdd.article.api.feign;

import com.pdd.article.api.fallback.Blog_Article_TypeFallback;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author:liyangpeng
 * @date:2019/6/21 10:16
 */
@FeignClient(value = "pdd-blog-article-server-v1", path = "blog_article_type", fallback = Blog_Article_TypeFallback.class)
public interface API_Blog_Article_Type {

    @GetMapping("/getArticleType")
    ResponseEntity getArticleType();
}
