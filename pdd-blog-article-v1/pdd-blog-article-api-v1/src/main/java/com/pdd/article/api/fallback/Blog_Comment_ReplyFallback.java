package com.pdd.article.api.fallback;

import com.pdd.article.api.feign.API_Blog_Comment_Reply;
import com.pdd.article.enums.WebResultEmun;
import com.pdd.article.response.ResponseResult;
import com.pdd.commons.entity.dto.ReplyCommentForm;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.validation.Valid;

/**
 * @author:liyangpeng
 * @date:2019/7/5 15:49
 */
@Component
public class Blog_Comment_ReplyFallback implements API_Blog_Comment_Reply {
    @Override
    public ResponseEntity replyComment(@Valid ReplyCommentForm replyCommentForm) {
        return ResponseResult.Return(WebResultEmun.SERVER_INVOKE_COMMENT_REPLY_ERROR);
    }
}
