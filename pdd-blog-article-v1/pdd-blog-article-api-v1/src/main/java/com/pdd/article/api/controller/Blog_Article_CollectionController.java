package com.pdd.article.api.controller;

import com.pdd.article.api.feign.API_Blog_Article_Collection;
import com.pdd.commons.entity.dto.ArticleCollectionFrom;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author:liyangpeng
 * @date:2019/7/9 10:35
 */
@RestController
@RequestMapping("/blog_article_collection")
public class Blog_Article_CollectionController {

    @Autowired
    private API_Blog_Article_Collection blog_article_collection;

    @PostMapping("/addCollection")
    public ResponseEntity addCollection(@Valid @RequestBody ArticleCollectionFrom articleCollectionFrom) {
        return blog_article_collection.addCollection(articleCollectionFrom);
    }
}
