package com.pdd.article.api.controller;

import com.pdd.article.api.feign.API_Blog_Article;
import com.pdd.commons.entity.dto.PublishArticleForm;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author:liyangpeng
 * @date:2019/6/19 11:35
 */
@RestController
@RequestMapping("/blog_article")
public class Blog_ArticleController {

    @Autowired
    private API_Blog_Article blog_article;

    @PostMapping("/publishArticle")
    public ResponseEntity publishArticle(@Valid @RequestBody PublishArticleForm publishArticleForm) {
        return blog_article.publishArticle(publishArticleForm);
    }
}
