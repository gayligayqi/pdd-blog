package com.pdd.article.api.feign;

import com.pdd.article.api.fallback.Blog_Article_ZanFallback;
import com.pdd.commons.entity.dto.ArticleZanFrom;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

/**
 * @author:liyangpeng
 * @date:2019/7/9 9:44
 */
@FeignClient(value = "pdd-blog-article-server-v1", path = "blog_article_zan", fallback = Blog_Article_ZanFallback.class)
public interface API_Blog_Article_Zan {

    @PostMapping("/addArticleZan")
    ResponseEntity addArticleZan(@RequestBody ArticleZanFrom articleZanFrom);
}
