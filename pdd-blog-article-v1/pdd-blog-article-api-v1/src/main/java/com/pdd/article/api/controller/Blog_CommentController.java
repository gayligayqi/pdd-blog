package com.pdd.article.api.controller;

import com.alibaba.fastjson.JSONObject;
import com.pdd.article.api.feign.API_Blog_Comment;
import com.pdd.commons.entity.dto.AddCommentFrom;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author:liyangpeng
 * @date:2019/7/4 20:10
 */
@RestController
@RequestMapping("/blog_comment")
public class Blog_CommentController {

    @Autowired
    private API_Blog_Comment blog_comment;

    /**
     * 发布评论的接口
     * @param addCommentFrom
     * @return
     */
    @PostMapping("/addComment")
    public ResponseEntity addComment(@Valid @RequestBody AddCommentFrom addCommentFrom) {
        return blog_comment.addComment(addCommentFrom);
    }
    /**
     * 用户评论管理查询评论
     * @return
     */
    @GetMapping("/getComment")
    public ResponseEntity getComment(Integer pageIndex, Integer pageSize){
        return blog_comment.getComment(pageIndex,pageSize);
    }
    /**
     * 删除评论
     * @param commentId
     * @return
     */
    @PostMapping("/delComment")
    public ResponseEntity delComment(@RequestBody JSONObject jsonObject){
        System.out.println(jsonObject.toJSONString());
        return blog_comment.delComment(jsonObject);
    }
}
