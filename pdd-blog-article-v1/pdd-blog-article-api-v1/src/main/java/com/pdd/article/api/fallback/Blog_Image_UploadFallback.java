package com.pdd.article.api.fallback;

import com.pdd.article.api.feign.API_Blog_Image_Upload;
import com.pdd.article.enums.WebResultEmun;
import com.pdd.article.response.ResponseResult;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author:liyangpeng
 * @date:2019/6/19 11:55
 */
@Component
public class Blog_Image_UploadFallback implements API_Blog_Image_Upload {

    @Override
    public ResponseEntity uploadImage(MultipartFile file, Integer type) {
        return ResponseResult.Return(WebResultEmun.SERVER_INVOKE_IMAGE_UPLOAD_ERROR);
    }
}
