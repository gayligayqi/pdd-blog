package com.pdd.article.api.controller;

import com.pdd.article.api.feign.API_Blog_Comment_Reply;
import com.pdd.commons.entity.dto.ReplyCommentForm;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author:liyangpeng
 * @date:2019/7/5 15:49
 */
@RestController
@RequestMapping("/blog_comment_reply")
public class Blog_Comment_ReplyController {

    @Autowired
    private API_Blog_Comment_Reply blog_comment_reply;

    @PostMapping("/replyComment")
    public ResponseEntity replyComment(@Valid @RequestBody ReplyCommentForm replyCommentForm) {
        return blog_comment_reply.replyComment(replyCommentForm);
    }
}
