package com.pdd.article.api.controller;

import com.pdd.article.api.feign.API_Blog_Image_Upload;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author:liyangpeng
 * @date:2019/6/19 11:55
 */
@RestController
@RequestMapping("/image_upload")
public class Blog_Image_UploadController {

    @Autowired
    private API_Blog_Image_Upload blog_image_upload;

    @PostMapping("/uploadImage")
    public ResponseEntity uploadImage(MultipartFile file, Integer type){
        return blog_image_upload.uploadImage(file,type);
    }
}
