package com.pdd.article.api.fallback;

import com.pdd.article.api.feign.API_Blog_Article;
import com.pdd.article.enums.WebResultEmun;
import com.pdd.article.response.ResponseResult;
import com.pdd.commons.entity.dto.PublishArticleForm;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.validation.Valid;

/**
 * @author:liyangpeng
 * @date:2019/6/19 11:35
 */
@Component
public class Blog_ArticleFallback implements API_Blog_Article {
    @Override
    public ResponseEntity publishArticle(@Valid PublishArticleForm publishArticleForm) {
        return ResponseResult.Return(WebResultEmun.SERVER_INVOKE_PUBLISH_ARTICLE_ERROR);
    }
}
