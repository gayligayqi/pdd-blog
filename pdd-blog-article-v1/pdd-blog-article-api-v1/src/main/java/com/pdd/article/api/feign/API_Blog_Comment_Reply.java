package com.pdd.article.api.feign;

import com.pdd.article.api.fallback.Blog_Comment_ReplyFallback;
import com.pdd.commons.entity.dto.ReplyCommentForm;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

/**
 * @author:liyangpeng
 * @date:2019/7/5 15:49
 */
@FeignClient(value = "pdd-blog-article-server-v1", path = "blog_comment_reply", fallback = Blog_Comment_ReplyFallback.class)
public interface API_Blog_Comment_Reply {

    @PostMapping("/replyComment")
    ResponseEntity replyComment(@RequestBody ReplyCommentForm replyCommentForm);
}
