package com.pdd.article.api.controller;

import com.pdd.article.api.feign.API_Blog_Comment_Zan;
import com.pdd.commons.entity.dto.CommentZanFrom;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author:liyangpeng
 * @date:2019/7/5 10:54
 */
@RestController
@RequestMapping("/blog_comment_zan")
public class Blog_Comment_ZanController {

    @Autowired
    private API_Blog_Comment_Zan blog_comment_zan;

    @PostMapping("/clickZan")
    public ResponseEntity clickZan(@RequestBody @Valid CommentZanFrom clickZanFrom){
        return blog_comment_zan.clickZan(clickZanFrom);
    };
}
