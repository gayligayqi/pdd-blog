package com.pdd.article.api.feign;

import com.pdd.article.api.fallback.Blog_Comment_ZanFallback;
import com.pdd.commons.entity.dto.CommentZanFrom;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

/**
 * @author:liyangpeng
 * @date:2019/7/5 10:54
 */
@FeignClient(value = "pdd-blog-article-server-v1", path = "blog_comment_zan", fallback = Blog_Comment_ZanFallback.class)
public interface API_Blog_Comment_Zan {

    @PostMapping("/clickZan")
    ResponseEntity clickZan(@RequestBody CommentZanFrom clickZanFrom);
}
