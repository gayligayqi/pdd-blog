package com.pdd.article.api.fallback;

import com.pdd.article.api.feign.API_Blog_Article_Type;
import com.pdd.article.enums.WebResultEmun;
import com.pdd.article.response.ResponseResult;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.stereotype.Component;

/**
 * @author:liyangpeng
 * @date:2019/6/21 10:16
 */
@Component
public class Blog_Article_TypeFallback implements API_Blog_Article_Type {
    @Override
    public ResponseEntity getArticleType() {
        return ResponseResult.Return(WebResultEmun.SERVER_INVOKE_ARTICLE_TYPE_ERROR);
    }
}
