package com.pdd.article.api.fallback;

import com.pdd.article.api.feign.API_Blog_Article_Collection;
import com.pdd.article.enums.WebResultEmun;
import com.pdd.article.response.ResponseResult;
import com.pdd.commons.entity.dto.ArticleCollectionFrom;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.validation.Valid;

/**
 * @author:liyangpeng
 * @date:2019/7/9 10:35
 */
@Component
public class Blog_Article_CollectionFallback implements API_Blog_Article_Collection {
    @Override
    public ResponseEntity addCollection(@Valid ArticleCollectionFrom articleCollectionFrom) {
        return ResponseResult.Return(WebResultEmun.SERVER_INVOKE_COLLECTION_ERROR);
    }
}
