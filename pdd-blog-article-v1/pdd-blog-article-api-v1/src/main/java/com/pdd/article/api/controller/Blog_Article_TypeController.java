package com.pdd.article.api.controller;

import com.pdd.article.api.feign.API_Blog_Article_Type;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author:liyangpeng
 * @date:2019/6/21 10:16
 */
@RestController
@RequestMapping("/blog_article_type")
public class Blog_Article_TypeController {

    @Autowired
    private API_Blog_Article_Type blog_article_type;

    @GetMapping("/getArticleType")
    public ResponseEntity getArticleType() {
        return blog_article_type.getArticleType();
    }
}
