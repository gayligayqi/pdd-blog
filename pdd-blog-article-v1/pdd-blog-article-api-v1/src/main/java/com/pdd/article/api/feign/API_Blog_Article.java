package com.pdd.article.api.feign;

import com.pdd.commons.entity.dto.PublishArticleForm;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author:liyangpeng
 * @date:2019/6/19 11:35
 */
@FeignClient(value = "pdd-blog-article-server-v1", path = "blog_article")
public interface API_Blog_Article {

    @PostMapping(value="/publishArticle",consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity publishArticle(@RequestBody PublishArticleForm publishArticleForm);

}
