package com.pdd.article.api.controller;

import com.pdd.article.api.feign.API_Blog_Article_Zan;
import com.pdd.commons.entity.dto.ArticleZanFrom;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author:liyangpeng
 * @date:2019/7/9 9:44
 */
@RestController
@RequestMapping("/blog_article_zan")
public class Blog_Article_ZanController {

    @Autowired
    private API_Blog_Article_Zan blog_article_zan;

    @PostMapping("/addArticleZan")
    public ResponseEntity addArticleZan(@Valid ArticleZanFrom articleZanFrom) {
        return blog_article_zan.addArticleZan(articleZanFrom);
    }
}
