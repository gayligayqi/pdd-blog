package com.pdd.article.api.fallback;

import com.pdd.article.api.feign.API_Blog_Article_Zan;
import com.pdd.article.enums.WebResultEmun;
import com.pdd.article.response.ResponseResult;
import com.pdd.commons.entity.dto.ArticleZanFrom;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.validation.Valid;

/**
 * @author:liyangpeng
 * @date:2019/7/9 9:44
 */
@Component
public class Blog_Article_ZanFallback implements API_Blog_Article_Zan {

    @Override
    public ResponseEntity addArticleZan(@Valid ArticleZanFrom articleZanFrom) {
        return ResponseResult.Return(WebResultEmun.SERVER_INVOKE_ARTICLE_TYPE_ERROR);
    }
}
