package com.pdd.article.api.controller.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * @author:liyangpeng
 * 静态资源映射
 * @date:2019/2/25 10:42
 */
@Configuration("articleResourceAdapterConfig")
public class ResourceAdapterConfig extends WebMvcConfigurerAdapter {

    @Autowired
    private UploadConfig uploadConfig;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler(uploadConfig.getAccess()+"/**").addResourceLocations("file:"+uploadConfig.getSavePath()+"/");
    }
}
