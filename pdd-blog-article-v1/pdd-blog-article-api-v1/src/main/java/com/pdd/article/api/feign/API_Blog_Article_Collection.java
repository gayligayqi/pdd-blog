package com.pdd.article.api.feign;

import com.pdd.article.api.fallback.Blog_Article_CollectionFallback;
import com.pdd.commons.entity.dto.ArticleCollectionFrom;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

/**
 * @author:liyangpeng
 * @date:2019/7/9 10:35
 */
@FeignClient(value = "pdd-blog-article-server-v1", path = "blog_article_collection", fallback = Blog_Article_CollectionFallback.class)
public interface API_Blog_Article_Collection {
    /**
     * 收藏
     * @param articleCollectionFrom
     * @return
     */
    @PostMapping("/addCollection")
    ResponseEntity addCollection(@RequestBody ArticleCollectionFrom articleCollectionFrom);
}
