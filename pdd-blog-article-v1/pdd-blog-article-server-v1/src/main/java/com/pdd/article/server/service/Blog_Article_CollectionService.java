package com.pdd.article.server.service;

import com.pdd.commons.entity.dto.ArticleCollectionFrom;
import com.pdd.commons.response.ResponseEntity;

/**
 * @author:liyangpeng
 * @date:2019/7/9 10:33
 */
public interface Blog_Article_CollectionService {

    ResponseEntity addCollection(ArticleCollectionFrom articleCollectionFrom);
}
