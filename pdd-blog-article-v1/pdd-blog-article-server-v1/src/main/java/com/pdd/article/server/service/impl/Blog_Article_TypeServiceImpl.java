package com.pdd.article.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.pdd.article.response.ResponseResult;
import com.pdd.article.server.mapper.Blog_Article_TypeMapper;
import com.pdd.article.server.service.Blog_Article_TypeService;
import com.pdd.commons.entity.dao.Blog_Article_Type;
import com.pdd.commons.response.ResponseEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author:liyangpeng
 * @date:2019/6/21 10:13
 */
@Service
@Slf4j
public class Blog_Article_TypeServiceImpl implements Blog_Article_TypeService {

    @Autowired
    private Blog_Article_TypeMapper blog_article_typeMapper;

    @Override
    public ResponseEntity getArticleType() {
        QueryWrapper<Blog_Article_Type> queryWrapper=new QueryWrapper<Blog_Article_Type>().ne("disable","0");
        List<Blog_Article_Type> results=blog_article_typeMapper.selectList(queryWrapper);
        return ResponseResult.Return(results);
    }

}
