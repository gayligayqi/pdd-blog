package com.pdd.article.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pdd.commons.entity.dao.Blog_Comment_Reply;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author:liyangpeng
 * @date:2019/7/5 15:43
 */
@Mapper
public interface Blog_Comment_ReplyMapper extends BaseMapper<Blog_Comment_Reply> {

}
