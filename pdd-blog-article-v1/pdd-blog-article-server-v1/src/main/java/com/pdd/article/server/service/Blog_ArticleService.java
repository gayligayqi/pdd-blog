package com.pdd.article.server.service;

import com.pdd.commons.entity.dto.PublishArticleForm;
import com.pdd.commons.response.ResponseEntity;

/**
 * @author:liyangpeng
 * @date:2019/6/19 11:21
 */
public interface Blog_ArticleService {

    ResponseEntity publishArticle(PublishArticleForm publishArticleForm);
}
