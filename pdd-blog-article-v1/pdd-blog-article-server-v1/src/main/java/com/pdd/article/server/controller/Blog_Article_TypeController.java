package com.pdd.article.server.controller;

import com.pdd.article.api.feign.API_Blog_Article_Type;
import com.pdd.article.server.service.Blog_Article_TypeService;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author:liyangpeng
 * @date:2019/6/21 10:16
 */
@RestController
@RequestMapping("/blog_article_type")
public class Blog_Article_TypeController implements API_Blog_Article_Type {

    @Autowired
    private Blog_Article_TypeService blog_article_typeService;

    @Override
    public ResponseEntity getArticleType() {
        return blog_article_typeService.getArticleType();
    }
}
