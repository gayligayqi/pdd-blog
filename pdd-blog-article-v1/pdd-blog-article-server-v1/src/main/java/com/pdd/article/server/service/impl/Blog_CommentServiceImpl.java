package com.pdd.article.server.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pdd.article.enums.WebResultEmun;
import com.pdd.article.response.ResponseResult;
import com.pdd.article.server.commons.util.OkHttpUtil;
import com.pdd.article.server.commons.yml.InterfaceConfig;
import com.pdd.article.server.mapper.Blog_CommentMapper;
import com.pdd.article.server.model.article.impl.ArticleProcessImpl;
import com.pdd.article.server.service.Blog_CommentService;
import com.pdd.commons.entity.dao.Blog_Account;
import com.pdd.commons.entity.dao.Blog_Comment;
import com.pdd.commons.entity.dto.AddCommentFrom;
import com.pdd.commons.entity.vo.AccountInfo_Vo;
import com.pdd.commons.response.ResponseEntity;
import com.pdd.commons.utils.AccountUtil;
import com.pdd.commons.utils.UserAgentUtils;
import com.pdd.commons.utils.WebAreaUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * @author:liyangpeng
 * @date:2019/7/4 20:10
 */
@Service
@Slf4j
public class Blog_CommentServiceImpl implements Blog_CommentService {

    @Autowired
    private Blog_CommentMapper blog_commentMapper;

    @Autowired
    private InterfaceConfig interfaceConfig;

    @Autowired
    private ArticleProcessImpl articleProcess;

    @Autowired
    private AccountUtil accountUtil;

    @Autowired
    private WebAreaUtil webAreaUtil;

    @Override
    public ResponseEntity addComment(AddCommentFrom addCommentFrom) {
        HttpServletRequest request=webAreaUtil.getRequest();
        String ip= UserAgentUtils.getIp(request);
        if(StringUtils.isEmpty(ip)){
            //未获取到ip
            return ResponseResult.Return(WebResultEmun.ADD_COMMENT_GET_IP_ERROR);
        }
        //调用百度ip解析接口 并获取用户ip归属地以及运营商
        String Result= OkHttpUtil.get(interfaceConfig.getBaiduIp()+ip,null,null,null);
        JSONObject baidu= JSON.parseObject(Result);
        JSONObject baiduData=baidu.getJSONArray("data").getJSONObject(0);
        //xss过滤
        addCommentFrom.setContent(articleProcess.XSSClean(addCommentFrom.getContent()));
        Blog_Comment blog_comment=new Blog_Comment();
        Blog_Account blog_account=new Blog_Account();
        BeanUtils.copyProperties(addCommentFrom,blog_comment);
        BeanUtils.copyProperties(accountUtil.getLoginInfo(),blog_account);
        blog_comment.setIp(ip);
        blog_comment.setLocation(baiduData.getString("location"));
        blog_comment.setPlatform(request.getHeader("platform"));
        blog_comment.setOsName(UserAgentUtils.getOsName(request));
        blog_comment.setBrowser(UserAgentUtils.getBrowserName(request));
        blog_comment.setPublishTime(new Date());
        blog_comment.setArticleId(addCommentFrom.getContentId());
        blog_comment.setAuthor_id(blog_account.getId());
        blog_comment.setUser(blog_account);
        boolean addRSuccess=blog_comment.insert();
        if(addRSuccess){
            return ResponseResult.Return(WebResultEmun.ADD_COMMENT_SUCCESS,blog_comment);
        }
        return ResponseResult.Return(WebResultEmun.ADD_COMMENT_ERROR);
    }

    @Override
    public ResponseEntity getComment(Integer pageIndex,Integer pageSize) {
        AccountInfo_Vo accountInfo_vo=accountUtil.getLoginInfo();
        IPage<Blog_Comment> comments=blog_commentMapper.getComment(new Page<>(pageIndex==null?1:pageIndex,pageSize==null?10:pageSize),accountInfo_vo.getId());
        return ResponseResult.Return(comments);
    }

    @Override
    public ResponseEntity delComment(JSONObject jsonObject) {
        if(jsonObject.getInteger("commentId")==null){
            return ResponseResult.RETURN_ERROR("commentId不能为空");
        }
        Blog_Comment blog_comment=new Blog_Comment();
        blog_comment.setId(jsonObject.getInteger("commentId"));
        blog_comment.setIsdel(1);
        Boolean delResult=blog_comment.updateById();
        if(delResult){
            log.info("【pdd博客】删除评论:删除评论成功,id:{},author_id:{}",JSON.toJSONString(jsonObject),accountUtil.getLoginInfo().getId());
            return ResponseResult.Return(WebResultEmun.DELETE_COMMENT_SUCCESS);
        }
        return ResponseResult.Return(WebResultEmun.DELETE_COMMENT_ERROR);
    }
}
