package com.pdd.article.server.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.pdd.account.api.feign.API_Blog_Account;
import com.pdd.article.enums.WebResultEmun;
import com.pdd.article.response.ResponseResult;
import com.pdd.article.server.commons.util.OkHttpUtil;
import com.pdd.article.server.commons.yml.InterfaceConfig;
import com.pdd.article.server.mapper.Blog_Comment_ReplyMapper;
import com.pdd.article.server.model.article.impl.ArticleProcessImpl;
import com.pdd.article.server.service.Blog_Comment_ReplyService;
import com.pdd.commons.entity.dao.Blog_Account;
import com.pdd.commons.entity.dao.Blog_Comment_Reply;
import com.pdd.commons.entity.dto.ReplyCommentForm;
import com.pdd.commons.entity.vo.AccountInfo_Vo;
import com.pdd.commons.response.ResponseEntity;
import com.pdd.commons.utils.AccountUtil;
import com.pdd.commons.utils.UserAgentUtils;
import com.pdd.commons.utils.WebAreaUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * @author:liyangpeng
 * @date:2019/7/5 15:44
 */
@Service
@Slf4j
public class Blog_Comment_ReplyServiceImpl implements Blog_Comment_ReplyService {

    @Autowired
    private Blog_Comment_ReplyMapper blog_comment_replyMapper;

    @Autowired
    private AccountUtil accountUtil;

    @Autowired
    private InterfaceConfig interfaceConfig;

    @Autowired
    private ArticleProcessImpl articleProcess;

    @Autowired
    private WebAreaUtil webAreaUtil;
    @Autowired
    private API_Blog_Account api_blog_account;

    @Override
    public ResponseEntity replyComment(ReplyCommentForm replyCommentForm) {
        HttpServletRequest request=webAreaUtil.getRequest();
        String ip= UserAgentUtils.getIp(request);
        if(StringUtils.isEmpty(ip)){
            //未获取到ip
            return ResponseResult.Return(WebResultEmun.ADD_COMMENT_GET_IP_ERROR);
        }
        //调用百度ip解析接口 并获取用户ip归属地以及运营商
        String Result= OkHttpUtil.get(interfaceConfig.getBaiduIp()+ip,null,null,null);
        JSONObject baidu= JSON.parseObject(Result);
        JSONObject baiduData=baidu.getJSONArray("data").getJSONObject(0);

        //xss过滤
        replyCommentForm.setReplyText(articleProcess.XSSClean(replyCommentForm.getReplyText()));
        AccountInfo_Vo accountInfo_vo=accountUtil.getLoginInfo();
        //登录信息判断
        if(accountInfo_vo==null){
            return ResponseResult.Return(WebResultEmun.LOGIN_INFO_NOT_EXISTS);
        }
        //不能回复自己的留言
        if(replyCommentForm.getReply_author_id_s().equals(accountInfo_vo.getId())){
            return ResponseResult.Return(WebResultEmun.REPLY_SELF_COMMENT_ERROR);
        }
        Blog_Comment_Reply blog_comment_reply=new Blog_Comment_Reply();
        BeanUtils.copyProperties(replyCommentForm,blog_comment_reply);
        blog_comment_reply.setReply_author_id(accountInfo_vo.getId());
        blog_comment_reply.setIp(ip);
        blog_comment_reply.setLocation(baiduData.getString("location"));
        blog_comment_reply.setPlatform(request.getHeader("platform"));
        blog_comment_reply.setOsName(UserAgentUtils.getOsName(request));
        blog_comment_reply.setBrowser(UserAgentUtils.getBrowserName(request));
        blog_comment_reply.setReplyTime(new Date());
        Boolean reply_Result=blog_comment_reply.insert();
        Blog_Account blog_account=new Blog_Account();
        BeanUtils.copyProperties(accountUtil.getLoginInfo(),blog_account);
        //留言回复人
        blog_comment_reply.setUser(blog_account);
        //留言被回复人
        Blog_Account api_Reply_user=api_blog_account.getAuthorById(replyCommentForm.getReply_author_id_s());
        blog_comment_reply.setReply_user(api_Reply_user);
        if(reply_Result){
            return ResponseResult.Return(WebResultEmun.REPLY_COMMENT_SUCCESS,blog_comment_reply);
        }
        return ResponseResult.Return(WebResultEmun.REPLY_COMMENT_ERROR);
    }
}
