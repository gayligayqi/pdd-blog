package com.pdd.article.server.commons.cdn;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author:liyangpeng
 * @date:2019/6/4 14:05
 */
@Component
@Data
public class ApiConfig {

    @Value("${api.cdn.cdnAccessKey}")
    public String cdnAccessKey;

    @Value("${api.cdn.cdnSecretKey}")
    public String cdnSecretKey;

}
