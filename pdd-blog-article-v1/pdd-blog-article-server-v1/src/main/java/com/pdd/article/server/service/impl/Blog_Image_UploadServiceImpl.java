package com.pdd.article.server.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.pdd.article.enums.WebResultEmun;
import com.pdd.article.response.ResponseResult;
import com.pdd.article.server.commons.cdn.TCCloudUploadUtil;
import com.pdd.article.server.commons.util.FileNameUtil;
import com.pdd.article.server.commons.yml.UploadConfig;
import com.pdd.article.server.service.Blog_Image_UploadService;
import com.pdd.commons.response.ResponseEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

/**
 * @author:liyangpeng
 * @date:2019/6/19 13:49
 */
@Slf4j
@Service
public class Blog_Image_UploadServiceImpl implements Blog_Image_UploadService {

    @Autowired
    private UploadConfig uploadConfig;

    @Autowired
    private TCCloudUploadUtil tcCloudUploadUtil;

    @Override
    public ResponseEntity uploadImage(MultipartFile multipartFile, Integer type) {
        if(type==null){
            return ResponseResult.Return(WebResultEmun.FIELD_NOT_NULL_ERROR,null,"type");
        }
        if(multipartFile.isEmpty()){
            //图片内容为空
            return ResponseResult.Return(WebResultEmun.IMAGE_UPLOAD_CONTENT_EMPTY_ERROR);
        }
        if(!multipartFile.getContentType().equals("image/jpeg")&&multipartFile.equals("image/png")){
            //图片格式错误
            return ResponseResult.Return(WebResultEmun.IMAGE_CONTENTTYPE_ERROR);
        }
        switch (type){
            //封面
            case 1:
                 return uploadCover(multipartFile);
            //内容图
            case 2:
                 return uploadContentImage(multipartFile);
            //头像
            case 3:
                return uploadAvatar(multipartFile);
            //背景图
            case 4:
                return uploadHeadBak(multipartFile);
        }
        //未匹配到文件上传类型
        return ResponseResult.Return(WebResultEmun.IMAGE_UPLOAD_TYPE_NO_MATCH_ERROR);
    }
    /**
     * 上传封面直接传cdn
     * @param multipartFile
     * @return
     */
    public ResponseEntity uploadCover(MultipartFile multipartFile){
        JSONObject result=saveFile(multipartFile,uploadConfig.getCover(),true,uploadConfig.getKey());
        if(result==null){
            return ResponseResult.Return(WebResultEmun.IMAGE_UPLOAD_ERROR);
        }
        return ResponseResult.Return(result);
    }
    /**
     * 正文图片上传
     * @param multipartFile
     * @return
     */
    public ResponseEntity uploadContentImage(MultipartFile multipartFile){
        JSONObject result=saveFile(multipartFile, uploadConfig.getContent(),false,null);
        if(result==null){
            return ResponseResult.Return(WebResultEmun.IMAGE_UPLOAD_ERROR);
        }
        return ResponseResult.Return(result);
    }

    /**
     * 头像上传
     * @param multipartFile
     * @return
     */
    public ResponseEntity uploadAvatar(MultipartFile multipartFile){
        JSONObject result=saveFile(multipartFile,uploadConfig.getAvatar(),true,uploadConfig.getAvatarkey());
        if(result==null){
            return ResponseResult.Return(WebResultEmun.IMAGE_UPLOAD_ERROR);
        }
        return ResponseResult.Return(result);
    }

    /**
     * 背景图上传
     * @param multipartFile
     * @return
     */
    public ResponseEntity uploadHeadBak(MultipartFile multipartFile){
        JSONObject result=saveFile(multipartFile,uploadConfig.getHeadbak(),true,uploadConfig.getHeadbackkey());
        if(result==null){
            return ResponseResult.Return(WebResultEmun.IMAGE_UPLOAD_ERROR);
        }
        return ResponseResult.Return(result);
    }
    /**
     * 保存图片方法
     * @param multipartFile
     * @return
     */
    public JSONObject saveFile(MultipartFile multipartFile, String saveSubPath, boolean uploadToCdn,String cdnKey){
        JSONObject jsonObject=new JSONObject();
        String fileName=multipartFile.getOriginalFilename();
        String path= FileNameUtil.mkdir(uploadConfig.getSavePath()+saveSubPath);
        String saveFileName=FileNameUtil.getImageName(fileName.substring(fileName.lastIndexOf(".")));
        saveFileName=path+"/"+saveFileName;
        File file=new File(saveFileName);
        try {
            multipartFile.transferTo(file);
            String accessPath=uploadConfig.getAccesshost()+uploadConfig.getAccess()+saveFileName.replace(uploadConfig.getSavePath(),"");
            if(uploadToCdn){
                //转存cdn
                String key1=file.getAbsolutePath().replaceAll("\\\\", "/").replace(uploadConfig.getSavePath()+saveSubPath,"");
                accessPath=tcCloudUploadUtil.putObject(file.getAbsolutePath(),cdnKey+key1);
            }
            jsonObject.put("accessPath",accessPath);
            return jsonObject;
        } catch (IOException e) {
            log.error("【pdd博客-图片上传】图片图片上传失败:{}",e);
        }
        return null;
    }

}
