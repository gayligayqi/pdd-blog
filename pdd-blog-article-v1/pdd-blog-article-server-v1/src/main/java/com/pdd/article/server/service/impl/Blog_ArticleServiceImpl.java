package com.pdd.article.server.service.impl;

import com.pdd.article.enums.WebResultEmun;
import com.pdd.article.response.ResponseResult;
import com.pdd.article.server.model.article.ArticleProcess;
import com.pdd.article.server.service.Blog_ArticleService;
import com.pdd.commons.entity.dao.Blog_Account;
import com.pdd.commons.entity.dao.Blog_Article;
import com.pdd.commons.entity.dto.PublishArticleForm;
import com.pdd.commons.entity.vo.AccountInfo_Vo;
import com.pdd.commons.entity.vo.Article_Vo;
import com.pdd.commons.response.ResponseEntity;
import com.pdd.commons.utils.AccountUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author:liyangpeng
 * @date:2019/6/19 11:21
 */
@Service
@Slf4j
public class Blog_ArticleServiceImpl implements Blog_ArticleService {

    @Autowired
    private ArticleProcess articleProcess;

    @Autowired
    private AccountUtil accountUtil;

    @Override
    public ResponseEntity publishArticle(PublishArticleForm publishArticleForm) {
        //获取用户登录信息
        AccountInfo_Vo accountInfo_vo=accountUtil.getLoginInfo();
        //1.富文本转义
        //2.图片水印(上传双份+原图)
        ResponseEntity article_voResult=articleProcess.processArticle(publishArticleForm);
        if(!article_voResult.isSuccess()){
            return article_voResult;
        }
        Article_Vo article_vo=(Article_Vo)article_voResult.getData();
        Blog_Article blog_article=new Blog_Article();
        BeanUtils.copyProperties(article_vo,blog_article);
        blog_article.setIsdel(0);
        blog_article.setIspass(0);
        blog_article.setUserToping(0);
        blog_article.setAdminToping(0);
        blog_article.setCanComment(publishArticleForm.getCanComment());
        Blog_Account blog_account=new Blog_Account();
        BeanUtils.copyProperties(accountInfo_vo,blog_account);
        blog_article.setAuthor_id(blog_account.getId());
        boolean publishStat=blog_article.insert();
        if(publishStat){
            log.info("【pdd博客】文章发布成功,title:{},cacheKey:{}",article_vo.getTitle(),article_vo.getCacheKey());
            return ResponseResult.Return(WebResultEmun.PUBLISH_ARTICLE_SUCCESS);
        }else{
            log.info("【pdd博客】文章发布失败,title:{},cacheKey:{}",article_vo.getTitle(),article_vo.getCacheKey());
            return ResponseResult.Return(WebResultEmun.PUBLISH_AETICLE_ERROR);
        }
    }
}
