package com.pdd.article.server.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pdd.commons.entity.dao.Blog_Comment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author:liyangpeng
 * @date:2019/7/4 20:09
 */
@Mapper
public interface Blog_CommentMapper extends BaseMapper<Blog_Comment> {

    IPage<Blog_Comment> getComment(IPage<Blog_Comment> page,@Param("authorId") Integer authorId);
}
