package com.pdd.article.server.service;

import com.pdd.commons.response.ResponseEntity;

/**
 * @author:liyangpeng
 * @date:2019/6/21 10:12
 */
public interface Blog_Article_TypeService {

     ResponseEntity getArticleType();
}
