package com.pdd.article.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pdd.commons.entity.dao.Blog_Article_Type;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author:liyangpeng
 * @date:2019/6/21 10:11
 */
@Mapper
public interface Blog_Article_TypeMapper extends BaseMapper<Blog_Article_Type> {

}