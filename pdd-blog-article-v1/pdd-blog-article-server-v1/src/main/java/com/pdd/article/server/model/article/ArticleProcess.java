package com.pdd.article.server.model.article;

import com.pdd.commons.entity.dto.PublishArticleForm;
import com.pdd.commons.response.ResponseEntity;
import org.jsoup.nodes.Document;

/**
 * @author:liyangpeng
 * @date:2019/6/20 14:19
 */
public interface ArticleProcess {
    /**
     * 图片跟正文内容处理
     * @return
     */
    ResponseEntity processArticle(PublishArticleForm publishArticleForm);
    /**
     * 转义
     * @return
     */
    String XSSFilter(String html);

    /**
     * 图片处理
     * @return
     */
    ResponseEntity imageProcess(Document document, boolean isWmk);
}
