package com.pdd.article.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pdd.commons.entity.dao.Blog_Article_Zan;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author:liyangpeng
 * @date:2019/7/9 9:41
 */
@Mapper
public interface Blog_Article_ZanMapper extends BaseMapper<Blog_Article_Zan>  {

    Integer isClick(@Param("articleId") Integer articleId, @Param("author_id") Integer author_id);

}
