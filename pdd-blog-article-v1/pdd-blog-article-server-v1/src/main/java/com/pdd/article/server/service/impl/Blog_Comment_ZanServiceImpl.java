package com.pdd.article.server.service.impl;

import com.pdd.article.enums.WebResultEmun;
import com.pdd.article.response.ResponseResult;
import com.pdd.article.server.mapper.Blog_Comment_ZanMapper;
import com.pdd.article.server.service.Blog_Comment_ZanService;
import com.pdd.commons.entity.dao.Blog_Comment_Zan;
import com.pdd.commons.entity.dto.CommentZanFrom;
import com.pdd.commons.entity.vo.AccountInfo_Vo;
import com.pdd.commons.response.ResponseEntity;
import com.pdd.commons.utils.AccountUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author:liyangpeng
 * @date:2019/7/5 10:51
 */
@Service
@Slf4j
public class Blog_Comment_ZanServiceImpl implements Blog_Comment_ZanService {

    @Autowired
    private Blog_Comment_ZanMapper blog_comment_zanMapper;
    @Autowired
    private AccountUtil accountUtil;

    @Override
    public ResponseEntity clickZan(CommentZanFrom clickZanFrom) {
        AccountInfo_Vo accountInfo_vo=accountUtil.getLoginInfo();
        if(accountInfo_vo==null){
            return ResponseResult.Return(WebResultEmun.LOGIN_INFO_NOT_EXISTS);
        }
        //查询是否已经点赞过。点赞过则修改点赞状态
        Integer id=blog_comment_zanMapper.isClick(clickZanFrom.getCommentId(),accountInfo_vo.getId());
        Blog_Comment_Zan blog_comment_zan=new Blog_Comment_Zan();
        if(id!=null){
            blog_comment_zan.setId(id);
        }
        BeanUtils.copyProperties(clickZanFrom,blog_comment_zan);
        blog_comment_zan.setAuthor_id(accountInfo_vo.getId());
        Boolean insertOrUpdate=blog_comment_zan.insertOrUpdate();
        if(insertOrUpdate){
            return ResponseResult.Return(WebResultEmun.CLICK_ZAN_SUCCESS);
        }
        return ResponseResult.Return(WebResultEmun.CLICK_ZAN_ERROR);
    }
}
