package com.pdd.article.server.service.impl;

import com.pdd.article.enums.WebResultEmun;
import com.pdd.article.response.ResponseResult;
import com.pdd.article.server.mapper.Blog_Article_ZanMapper;
import com.pdd.article.server.service.Blog_Article_ZanService;
import com.pdd.commons.entity.dao.Blog_Article_Zan;
import com.pdd.commons.entity.dto.ArticleZanFrom;
import com.pdd.commons.entity.vo.AccountInfo_Vo;
import com.pdd.commons.response.ResponseEntity;
import com.pdd.commons.utils.AccountUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author:liyangpeng
 * @date:2019/7/9 9:43
 */
@Service
@Slf4j
public class Blog_Article_ZanServiceImpl implements Blog_Article_ZanService {

    @Autowired
    private Blog_Article_ZanMapper blog_article_zanMapper;
    @Autowired
    private AccountUtil accountUtil;

    @Override
    public ResponseEntity addArticleZan(ArticleZanFrom articleZanFrom) {
        AccountInfo_Vo accountInfo_vo=accountUtil.getLoginInfo();
        if(accountInfo_vo==null){
            return ResponseResult.Return(WebResultEmun.LOGIN_INFO_NOT_EXISTS);
        }
        //查询是否已经点赞过。点赞过则修改点赞状态
        Integer id=blog_article_zanMapper.isClick(articleZanFrom.getContentId(),accountInfo_vo.getId());
        Blog_Article_Zan blog_article_zan=new Blog_Article_Zan();
        blog_article_zan.setClickStatus(articleZanFrom.getClickStatus());
        blog_article_zan.setArticleId(articleZanFrom.getContentId());
        blog_article_zan.setAuthor_id(accountInfo_vo.getId());
        if(id!=null){
            blog_article_zan.setId(id);
        }
        blog_article_zan.setAuthor_id(accountInfo_vo.getId());
        Boolean insertOrUpdate=blog_article_zan.insertOrUpdate();
        if(insertOrUpdate){
            return ResponseResult.Return(WebResultEmun.CLICK_ZAN_SUCCESS);
        }
        return ResponseResult.Return(WebResultEmun.CLICK_ZAN_ERROR);
    }
}

