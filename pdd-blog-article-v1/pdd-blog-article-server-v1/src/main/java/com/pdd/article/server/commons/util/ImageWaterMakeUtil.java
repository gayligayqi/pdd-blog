package com.pdd.article.server.commons.util;

import lombok.extern.slf4j.Slf4j;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

@Slf4j
public class ImageWaterMakeUtil {
    // 水印透明度
    private static float alpha = 0.2f;
    // 水印文字颜色
    private static Color color = Color.white;
    // 水印之间的间隔
    private static final int XMOVE = 80;
    // 水印之间的间隔
    private static final int YMOVE = 80;
    //字体大小
    private static final int FONT_SIZE=16;
    //水印旋转
    private static final int DEGREE=-40;

    /**
     * 获取文本长度。汉字为1:1，英文和数字为2:1
     */
    private static int getTextLength (String text) {
        int length = text.length ();
        for (int i = 0; i < text.length (); i++) {
            String s = String.valueOf (text.charAt (i));
            if (s.getBytes ().length > 1) {
                length++;
            }
        }
        length = length % 2 == 0 ? length / 2 : length / 2 + 1;
        return length;
    }

    /**
     * 给图片添加水印文字
     *
     * @param logoText 水印文字
     * @param srcImgPath 源图片路径
     * @param targerPath 目标图片路径
     */
    public static String ImageByText (String logoText, String srcImgPath, String targerPath) {
        return ImageByText (logoText, srcImgPath, targerPath, DEGREE);
    }

    /**
     * 给图片添加水印文字、可设置水印文字的旋转角度
     *
     * @param logoText 水印文字
     * @param srcImgPath 原图
     * @param targerPath 水印输出图
     * @param degree 水印倾斜 -40 向左下倾斜
     */
    private static String ImageByText (String logoText, String srcImgPath, String targerPath, Integer degree) {
        // 源图片
        try{
            Image srcImg = ImageIO.read (new File(srcImgPath));
            // 原图宽度
            int width = srcImg.getWidth (null);
            // 原图高度
            int height = srcImg.getHeight (null);
            BufferedImage buffImg = new BufferedImage (srcImg.getWidth (null), srcImg.getHeight (null), BufferedImage.TYPE_INT_RGB);
            // 得到画笔对象
            Graphics2D g = buffImg.createGraphics ();
            // 设置对线段的锯齿状边缘处理
            g.setRenderingHint (RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            g.drawImage (srcImg.getScaledInstance (srcImg.getWidth (null), srcImg.getHeight (null), Image.SCALE_SMOOTH), 0, 0, null);
            // 设置水印旋转
            if (null != degree) {
                g.rotate (Math.toRadians (degree), (double) buffImg.getWidth () / 2, (double) buffImg.getHeight () / 2);
            }
            // 设置水印文字颜色
            g.setColor (color);
            Font font = new Font ("微软雅黑", Font.BOLD, FONT_SIZE);
            // 设置水印文字Font
            g.setFont (font);
            // 设置水印文字透明度
            g.setComposite (AlphaComposite.getInstance (AlphaComposite.SRC_ATOP, alpha));
            int x = -width / 2;
            int y = -height / 2;
            // 字体长度
            int markWidth = FONT_SIZE * getTextLength (logoText);
            //字体高度
            int markHeight = FONT_SIZE;

            // 循环添加水印
            while (x < width * 1.5) {
                y = -height / 2;
                while (y < height * 1.5) {
                    g.drawString (logoText, x, y);

                    y += markHeight + YMOVE;
                }
                x += markWidth + XMOVE;
            }
            // 释放资源
            g.dispose ();
            // 生成图片
            ImageIO.write (buffImg, "JPG", new File(targerPath));
            buffImg.flush();
            srcImg.flush();
            return targerPath;
        } catch (IOException e) {
            e.printStackTrace();
            log.error("【pdd博客】图片水印添加失败:{}",e);
        }
        return null;
    }
}
