package com.pdd.article.server.service;

import com.pdd.commons.response.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author:liyangpeng
 * @date:2019/6/19 13:48
 */
public interface Blog_Image_UploadService {

    ResponseEntity uploadImage(MultipartFile multipartFile, Integer type);

}
