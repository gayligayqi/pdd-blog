package com.pdd.article.server.commons.util;

/**
 * @author:liyangpeng
 * @date:2019/6/19 14:49
 */
public class StrUtil {

    /**
     * 6位随机数
     * @return
     */
    public static String getRandomBy6(){
        return String.valueOf((int)((Math.random()*9+1)*100000));
    }

    /**
     * long.MAX_VALUE-(13位时间戳+6位随机数)
     * @return
     */
    public  static String cache_key(){
        String cts=System.currentTimeMillis()+getRandomBy6();
        Long key=Long.MAX_VALUE-Long.valueOf(cts);
        return String.valueOf(key);
    }
}
