package com.pdd.article.server.model.image;

import com.pdd.commons.response.ResponseEntity;

/**
 * @author:liyangpeng
 * @date:2019/6/20 14:19
 */
public interface ImageProcess {

    ResponseEntity downLoadImage(String url);

    ResponseEntity compressedUpload(String url, boolean isWmk, String logText);
}
