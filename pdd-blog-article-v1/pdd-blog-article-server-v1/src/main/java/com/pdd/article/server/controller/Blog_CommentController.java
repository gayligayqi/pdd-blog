package com.pdd.article.server.controller;

import com.alibaba.fastjson.JSONObject;
import com.pdd.article.api.feign.API_Blog_Comment;
import com.pdd.article.server.service.Blog_CommentService;
import com.pdd.commons.entity.dto.AddCommentFrom;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author:liyangpeng
 * @date:2019/7/4 20:10
 */
@RestController
@RequestMapping("/blog_comment")
public class Blog_CommentController  implements API_Blog_Comment {

    @Autowired
    private Blog_CommentService blog_commentService;

    /**
     * 发布评论的接口
     * @param addCommentFrom
     * @return
     */
    @Override
    public ResponseEntity addComment(@Valid @RequestBody AddCommentFrom addCommentFrom) {
        return blog_commentService.addComment(addCommentFrom);
    }

    /**
     * 查询评论
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @Override
    public ResponseEntity getComment(Integer pageIndex, Integer pageSize) {
        return blog_commentService.getComment(pageIndex,pageSize);
    }

    /**
     * 删除评论
     * @param jsonObject
     * @return
     */
    @Override
    public ResponseEntity delComment(JSONObject jsonObject) {
        return blog_commentService.delComment(jsonObject);
    }
}
