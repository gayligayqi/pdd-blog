package com.pdd.article.server.service.impl;

import com.pdd.article.enums.WebResultEmun;
import com.pdd.article.response.ResponseResult;
import com.pdd.article.server.mapper.Blog_Article_CollectionMapper;
import com.pdd.article.server.service.Blog_Article_CollectionService;
import com.pdd.commons.entity.dao.Blog_Article_Collection;
import com.pdd.commons.entity.dto.ArticleCollectionFrom;
import com.pdd.commons.entity.vo.AccountInfo_Vo;
import com.pdd.commons.response.ResponseEntity;
import com.pdd.commons.utils.AccountUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author:liyangpeng
 * @date:2019/7/9 10:34
 */
@Service
@Slf4j
public class Blog_Article_CollectionServiceImpl implements Blog_Article_CollectionService {

    @Autowired
    private Blog_Article_CollectionMapper blog_article_collectionMapper;

    @Autowired
    private AccountUtil accountUtil;

    @Override
    public ResponseEntity addCollection(ArticleCollectionFrom articleCollectionFrom) {
        AccountInfo_Vo accountInfo_vo=accountUtil.getLoginInfo();
        if(accountInfo_vo==null){
            return ResponseResult.Return(WebResultEmun.LOGIN_INFO_NOT_EXISTS);
        }
        //查询是否已经收藏。收藏过则修改收藏状态
        Integer id=blog_article_collectionMapper.isClick(articleCollectionFrom.getContentId(),accountInfo_vo.getId());
        Blog_Article_Collection blog_article_collection=new Blog_Article_Collection();
        blog_article_collection.setCollStatus(articleCollectionFrom.getCollStatus());
        blog_article_collection.setArticleId(articleCollectionFrom.getContentId());
        blog_article_collection.setAuthor_id(accountInfo_vo.getId());
        if(id!=null){
            blog_article_collection.setId(id);
        }
        Boolean insertOrUpdate=blog_article_collection.insertOrUpdate();
        if(insertOrUpdate){
            return ResponseResult.Return(WebResultEmun.ARTICLE_COLLECTION_SUCCESS);
        }
        return ResponseResult.Return(WebResultEmun.ARTICLE_COLLECTION_ERROR);
    }
}
