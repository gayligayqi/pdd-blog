package com.pdd.article.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pdd.commons.entity.dao.Blog_Article;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author:liyangpeng
 * @date:2019/6/19 11:20
 */
@Mapper
public interface Blog_ArticleMapper extends BaseMapper<Blog_Article> {

}
