package com.pdd.article.server.commons.yml;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author:liyangpeng
 * @date:2019/6/19 14:23
 */
@Data
@Component
@ConfigurationProperties(prefix = "upload")
public class UploadConfig {

    private String savePath;

    private String cover;

    private String key;

    private String content;

    private String access;

    private String accesshost;

    private String compression;

    private String avatarkey;

    private String headbackkey;

    private String avatar;

    private String headbak;

}
