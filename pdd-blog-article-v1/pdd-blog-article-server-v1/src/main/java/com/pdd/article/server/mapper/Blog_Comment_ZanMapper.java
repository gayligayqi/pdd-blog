package com.pdd.article.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pdd.commons.entity.dao.Blog_Comment_Zan;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author:liyangpeng
 * @date:2019/7/5 10:38
 */
@Mapper
public interface Blog_Comment_ZanMapper extends BaseMapper<Blog_Comment_Zan> {

    Integer isClick(@Param("commentId") Integer commentId, @Param("author_id") Integer author_id);

}
