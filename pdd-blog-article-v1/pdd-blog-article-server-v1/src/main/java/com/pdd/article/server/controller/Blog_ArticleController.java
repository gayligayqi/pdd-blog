package com.pdd.article.server.controller;

import com.pdd.article.api.feign.API_Blog_Article;
import com.pdd.article.server.service.Blog_ArticleService;
import com.pdd.commons.entity.dto.PublishArticleForm;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author:liyangpeng
 * @date:2019/6/19 11:35
 */
@RestController
@RequestMapping("/blog_article")
public class Blog_ArticleController implements API_Blog_Article {

    @Autowired
    private Blog_ArticleService blog_articleService;

    @Override
    public ResponseEntity publishArticle(@Valid PublishArticleForm publishArticleForm) {
        return blog_articleService.publishArticle(publishArticleForm);
    }
}
