package com.pdd.article.server.commons.util;

import org.springframework.util.DigestUtils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author:liyangpeng
 * @date:2019/6/19 14:47
 */
public class FileNameUtil {

    /**
     * 生成文件名
     * 文件名规则：当前时间+6位随机数 MD5加密
     * @param suffix
     * @return
     */
    public static String getImageName(String suffix){
        long currentTime=System.currentTimeMillis();
        return DigestUtils.md5DigestAsHex((String.valueOf(currentTime)+StrUtil.getRandomBy6()).getBytes())+suffix;
    }

    /**
     * 日期子文件夹创建
     */
    public static String mkdir(String path){
        SimpleDateFormat format=new SimpleDateFormat("yyyyMMdd");
        String savePathDate=format.format(new Date());
        String Path=path+savePathDate;
        File file=new File(Path);
        //自动创建文件目录
        if(!file.exists()){
            file.mkdirs();
        }
        return Path;
    }
}
