package com.pdd.article.server.model.image.impl;

import com.alibaba.fastjson.JSONObject;
import com.pdd.article.response.ResponseResult;
import com.pdd.article.server.commons.cdn.TCCloudUploadUtil;
import com.pdd.article.server.commons.util.FileMd5Util;
import com.pdd.article.server.commons.util.FileNameUtil;
import com.pdd.article.server.commons.util.ImageWaterMakeUtil;
import com.pdd.article.server.commons.util.OkHttpUtil;
import com.pdd.article.server.commons.yml.UploadConfig;
import com.pdd.article.server.model.image.ImageProcess;
import com.pdd.commons.entity.vo.Blog_Image_Vo;
import com.pdd.commons.response.ResponseEntity;
import net.coobird.thumbnailator.Thumbnails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * @author:liyangpeng
 * @date:2019/6/20 14:22
 */
@Component
public class ImageProcessImpl implements ImageProcess {

    @Autowired
    private UploadConfig uploadConfig;
    @Autowired
    private TCCloudUploadUtil tcCloudUploadUtil;
    /**
     * 正文图片下载保存
     * @param url
     * @return
     */
    @Override
    public ResponseEntity downLoadImage(String url) {
        String suffix="";
        if(!url.toLowerCase().endsWith(".png")&&!url.toLowerCase().endsWith(".jpg")){
            suffix=".jpg";
        }else{
            suffix=url.substring(url.lastIndexOf(".")).isEmpty()?".jpg":url.substring(url.lastIndexOf("."));
        }
        String path= FileNameUtil.mkdir(uploadConfig.getSavePath()+uploadConfig.getContent());
        String saveFileName=FileNameUtil.getImageName(suffix);
        saveFileName=path+"/"+saveFileName;
        String result= OkHttpUtil.downLoadImg(url,saveFileName);
        //下载成功
        if(result.equals("success")){
            JSONObject object=new JSONObject();
            object.put("path",saveFileName);
            return ResponseResult.Return(object);
        }
        return ResponseResult.RETURN_ERROR("正文图片下载失败");
    }

    /**
     * 正文压缩上传
     * @return
     */
    @Override
    public ResponseEntity compressedUpload(String url, boolean isWmk, String logText) {
        Blog_Image_Vo blog_image_vo=new Blog_Image_Vo();
        String path=uploadConfig.getSavePath()+url.replace(uploadConfig.getAccesshost()+uploadConfig.getAccess(),"");
        String compression=FileNameUtil.mkdir(uploadConfig.getSavePath()+uploadConfig.getCompression())+path.substring(path.lastIndexOf("/"));
        try {
            //1.压缩
            Thumbnails.of(ImageIO.read(new File(path))).scale(0.8).toFile(compression);
            File file=new File(compression);
            BufferedImage bufferedImage=ImageIO.read(file);
            blog_image_vo.setWidth(bufferedImage.getWidth());
            blog_image_vo.setHeigth(bufferedImage.getHeight());
            blog_image_vo.setSize(file.length());
            blog_image_vo.setMd5(FileMd5Util.getMd5Code(file));
            //2.上传
            //压缩后的原图key
            String key=compression.replace(uploadConfig.getSavePath()+uploadConfig.getCompression(),"");
            String orimg=tcCloudUploadUtil.putObject(compression,uploadConfig.getKey()+key);
            blog_image_vo.setSrc(orimg);
            //3.水印
            if(isWmk){
                String endWith=compression.substring(compression.lastIndexOf("."));
                String wmkImg=compression.replace(endWith,"_wmk"+endWith);
                String logoText="gay哩gay气-"+logText;
                //添加水印
                ImageWaterMakeUtil.ImageByText(logoText,compression,wmkImg);
                //压缩后的水印图key
                String wmkkey=wmkImg.replace(uploadConfig.getSavePath()+uploadConfig.getCompression(),"");
                //2.1:上传双份wmkImg
                String wmkimg=tcCloudUploadUtil.putObject(wmkImg,uploadConfig.getKey()+wmkkey);
                blog_image_vo.setWmk(wmkimg);
            }
            bufferedImage.flush();
            return ResponseResult.Return(blog_image_vo);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ResponseResult.RETURN_ERROR("正文图片压缩上传失败");
    }
}
