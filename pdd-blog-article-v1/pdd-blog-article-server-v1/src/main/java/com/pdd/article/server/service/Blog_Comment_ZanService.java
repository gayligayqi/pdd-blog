package com.pdd.article.server.service;

import com.pdd.commons.entity.dto.CommentZanFrom;
import com.pdd.commons.response.ResponseEntity;

/**
 * @author:liyangpeng
 * @date:2019/7/5 10:51
 */
public interface Blog_Comment_ZanService {

    ResponseEntity clickZan(CommentZanFrom clickZanFrom);

}
