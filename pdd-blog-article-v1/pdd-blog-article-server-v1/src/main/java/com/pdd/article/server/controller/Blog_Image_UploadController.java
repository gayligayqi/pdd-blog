package com.pdd.article.server.controller;

import com.pdd.article.api.feign.API_Blog_Image_Upload;
import com.pdd.article.server.service.Blog_Image_UploadService;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author:liyangpeng
 * @date:2019/6/19 11:55
 */
@RestController
@RequestMapping("/image_upload")
public class Blog_Image_UploadController implements API_Blog_Image_Upload {

    @Autowired
    private Blog_Image_UploadService uploadService;

    @Override
    public ResponseEntity uploadImage(MultipartFile file, Integer type){
        return uploadService.uploadImage(file,type);
    }
}
