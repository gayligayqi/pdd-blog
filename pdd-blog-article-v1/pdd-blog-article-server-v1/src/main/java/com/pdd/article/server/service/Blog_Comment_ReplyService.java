package com.pdd.article.server.service;

import com.pdd.commons.entity.dto.ReplyCommentForm;
import com.pdd.commons.response.ResponseEntity;

/**
 * @author:liyangpeng
 * @date:2019/7/5 15:44
 */
public interface Blog_Comment_ReplyService {

    ResponseEntity replyComment(ReplyCommentForm replyCommentForm);

}
