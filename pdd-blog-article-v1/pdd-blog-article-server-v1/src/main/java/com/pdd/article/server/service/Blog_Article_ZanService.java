package com.pdd.article.server.service;

import com.pdd.commons.entity.dto.ArticleZanFrom;
import com.pdd.commons.response.ResponseEntity;

/**
 * @author:liyangpeng
 * @date:2019/7/9 9:42
 */
public interface Blog_Article_ZanService {

    ResponseEntity addArticleZan(ArticleZanFrom articleZanFrom);
}
