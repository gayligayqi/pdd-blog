package com.pdd.article.server.controller;

import com.pdd.article.api.feign.API_Blog_Article_Zan;
import com.pdd.article.server.service.Blog_Article_ZanService;
import com.pdd.commons.entity.dto.ArticleZanFrom;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author:liyangpeng
 * @date:2019/7/9 9:44
 */
@RestController
@RequestMapping("/blog_article_zan")
public class Blog_Article_ZanController implements API_Blog_Article_Zan {

    @Autowired
    private Blog_Article_ZanService blog_article_zanService;

    @Override
    public ResponseEntity addArticleZan(@Valid @RequestBody ArticleZanFrom articleZanFrom) {
        return blog_article_zanService.addArticleZan(articleZanFrom);
    }
}
