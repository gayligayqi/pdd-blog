package com.pdd.article.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author:liyangpeng
 * @date:2019/7/11 11:13
 */
@SpringBootApplication(scanBasePackages = {"com.pdd.article.server","com.pdd.commons"},excludeName ="com.pdd.article.api")
@EnableEurekaClient
@EnableFeignClients(basePackages={"com.pdd.account.api","com.pdd.feign"})
public class PddBlogArticleServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(PddBlogArticleServerApplication.class,args);
    }
}
