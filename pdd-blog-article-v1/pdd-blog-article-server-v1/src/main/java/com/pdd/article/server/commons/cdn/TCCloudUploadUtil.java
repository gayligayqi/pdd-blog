package com.pdd.article.server.commons.cdn;

import com.qcloud.cos.COSClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;

/**
 * @author:liyangpeng
 * @date:2019/6/3 17:30
 */
@Component
public class TCCloudUploadUtil {

    @Value("${cdn.accessdomain}")
    private String accessdomain;

    @Value("${cdn.bucket}")
    private String bucket;

    @Autowired
    private TCCloudFactory tcCloudFactory;

    /**
     * 单文件上传返回访问地址
     * @param localPath
     * @param key
     * @return
     */
    public String putObject(String localPath,String key){
        COSClient cosClient=tcCloudFactory.getInstance();
        cosClient.putObject(bucket,key,new File(localPath));
        return accessdomain+key;
    }
}
