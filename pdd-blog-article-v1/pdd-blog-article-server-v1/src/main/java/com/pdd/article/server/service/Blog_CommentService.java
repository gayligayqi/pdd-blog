package com.pdd.article.server.service;

import com.alibaba.fastjson.JSONObject;
import com.pdd.commons.entity.dto.AddCommentFrom;
import com.pdd.commons.response.ResponseEntity;

/**
 * @author:liyangpeng
 * @date:2019/7/4 20:10
 */
public interface Blog_CommentService {

    ResponseEntity addComment(AddCommentFrom addCommentFrom);

    ResponseEntity getComment(Integer pageIndex,Integer pageSize);

    ResponseEntity delComment(JSONObject jsonObject);
}
