package com.pdd.article.response;

import com.pdd.article.enums.WebResultEmun;
import com.pdd.commons.response.ResponseEntity;

/**
 * @author:liyangpeng
 * @date:2018/12/7 15:01
 */
public class ResponseResult extends com.pdd.commons.response.ResponseResult {


    public static ResponseEntity Return(WebResultEmun emun){
        return Return(emun,null,null);
    }

    public static ResponseEntity Return(Object data){
        return Return(WebResultEmun.SUCCESS,data,null);
    }

    public static ResponseEntity Return(WebResultEmun emun, Object data){
        return Return(emun,data,null);
    }
    /**
     * 返回请求,携带格式字符串的
     * @param emun
     * @param str
     * @return
     */
    public static ResponseEntity Return(WebResultEmun emun, Object data, String ...str){
        ResponseEntity entity=new ResponseEntity();
        entity.setCode(emun.getCode());
        if(str!=null){
            entity.setMessage(String.format(emun.getMsg(),str));
        }else{
            entity.setMessage(emun.getMsg());
        }
        entity.setData(data);
        entity.setSuccess(emun.isSuccess());
        return entity;
    }
}
