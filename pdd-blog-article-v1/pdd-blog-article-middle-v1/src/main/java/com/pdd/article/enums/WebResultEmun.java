package com.pdd.article.enums;

/**
 * @author:liyangpeng
 * @date:2019/7/11 16:26
 */
public enum WebResultEmun {
    LOGIN_INFO_NOT_EXISTS("0001","登录信息已失效,请重新登录,错误编码:PBA_0001",false),
    SUCCESS("PBA_0000","请求成功",true),
    IMAGE_UPLOAD_ERROR("PBA_I0000","上传失败",false),
    IMAGE_CONTENTTYPE_ERROR("PBA_I0001","图片格式错误",false),
    IMAGE_UPLOAD_CONTENT_EMPTY_ERROR("PBA_I0002","文件内容为空",false),
    IMAGE_UPLOAD_TYPE_NO_MATCH_ERROR("PBA_I0003","未匹配到图片上传类型",false),
    FIELD_NOT_NULL_ERROR("PBA_FD0001","%s不能为空",false),
    CONTENT_XSS_FILTER_EMPTY_ERROR("PBA_C0001","正文内容为空",false),
    PUBLISH_ARTICLE_SUCCESS("PBA_I0000","发布成功",true),
    PUBLISH_AETICLE_ERROR("PBA_I00004","发布失败",false),
    ADD_COMMENT_GET_IP_ERROR("PBA_AC0001","获取用户ip失败",false),
    ADD_COMMENT_SUCCESS("PBA_AC0000","发表评论成功",true),
    ADD_COMMENT_ERROR("PBA_AC0002","发表评论失败",false),
    DELETE_COMMENT_SUCCESS("PBA_AC0003","删除评论成功",true),
    DELETE_COMMENT_ERROR("PBA_AC0004","删除评论失败",false),
    CLICK_ZAN_ERROR("PBA_Z0001","点赞失败",false),
    CLICK_ZAN_SUCCESS("PBA_Z0000","点赞成功",true),
    REPLY_COMMENT_SUCCESS("PBA_RC0000","回复留言成功",true),
    REPLY_SELF_COMMENT_ERROR("PDA_RC0001","不能回复自己的留言哦",false),
    REPLY_COMMENT_ERROR("PBA_RC0002","回复留言失败",false),
    ARTICLE_COLLECTION_SUCCESS("PBA_ACO0000","收藏成功",true),
    ARTICLE_COLLECTION_ERROR("PBA_ACO0001","收藏失败",false),
    SERVER_INVOKE_COLLECTION_ERROR("PBA_SIE0001","收藏失败,错误码:PBA_SIE0001",false),
    SERVER_INVOKE_ARTICLE_TYPE_ERROR("PBA_SIE0002","查询文章发布类型失败,错误码:PBA_SIE0002",false),
    SERVER_INVOKE_PUBLISH_ARTICLE_ERROR("PBA_SIE0003","文章发布失败,错误码:PBA_SIE0003",false),
    SERVER_INVOKE_COMMENT_REPLY_ERROR("PBA_SIE0004","回复评论失败,错误码:PBA_SIE0004",false),
    SERVER_INVOKE_COMMENT_ZAN_ERROR("PBA_SIE0005","评论点赞失败,错误码:PBA_SIE0005",false),
    SERVER_INVOKE_ADD_COMMENT_ERROR("PBA_SIE0006","发表评论失败,错误码:PBA_SIE0006",false),
    SERVER_INVOKE_IMAGE_UPLOAD_ERROR("PBA_SIE0007","图片上传失败,错误码:PBA_SIE0007",false),
    SERVER_INVOKE_GETCOMMENT_ERROR("PBA_SIE0008","查询评论失败,错误码:PBA_SIE0008",false),
    SERVER_INVOKE_DELETE_COMMENT_ERROR("PBA_SIE0009","删除评论失败,错误码:PBA_SIE0009",false)
    ;

    private String code;
    private String msg;
    private boolean success;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    WebResultEmun(String code, String msg, boolean success) {
        this.code = code;
        this.msg = msg;
        this.success=success;
    }
}
