package com.pdd.account.tourist.api.feign;

import com.pdd.account.tourist.api.fallback.Blog_Zone_AccessFallback;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author:liyangpeng
 * @date:2019/7/16 9:39
 */
@FeignClient(value = "pdd-blog-account-tourist-server-v1", path = "blog_zone_access", fallback = Blog_Zone_AccessFallback.class)
public interface API_Blog_Zone_Access {

    /**
     * 查询用户主页访问历史
     * @param zoneId
     * @return
     */
    @GetMapping("/getZoneAccess")
    ResponseEntity getZoneAccess(@RequestParam("zoneId") Integer zoneId);
}
