package com.pdd.account.tourist.api.feign;

import com.pdd.account.tourist.api.fallback.Blog_Account_AchieveMent_LogFallback;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author:liyangpeng
 * @date:2019/7/16 14:21
 */
@FeignClient(value = "pdd-blog-account-tourist-server-v1", path = "achieveMent", fallback = Blog_Account_AchieveMent_LogFallback.class)
public interface API_Blog_Account_AchieveMent_Log {

    @GetMapping("/getAccountAchiveMentLog")
    ResponseEntity getAccountAchiveMentLog(@RequestParam("author_id") Integer author_id);

}
