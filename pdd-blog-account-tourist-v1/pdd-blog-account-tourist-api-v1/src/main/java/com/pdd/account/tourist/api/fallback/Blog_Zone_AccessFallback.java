package com.pdd.account.tourist.api.fallback;

import com.pdd.account.tourist.api.feign.API_Blog_Zone_Access;
import com.pdd.account.tourist.enums.WebResultEmun;
import com.pdd.account.tourist.response.ResponseResult;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.stereotype.Component;

/**
 * @author:liyangpeng
 * @date:2019/7/16 9:40
 */
@Component
public class Blog_Zone_AccessFallback implements API_Blog_Zone_Access {

    @Override
    public ResponseEntity getZoneAccess(Integer zoneId) {
        return ResponseResult.Return(WebResultEmun.SERVER_INVOKE_ZONE_ACCESS_ERROR);
    }
}
