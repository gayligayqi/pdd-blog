package com.pdd.account.tourist.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author:liyangpeng
 * @date:2019/7/11 16:32
 */
@SpringBootApplication(scanBasePackages ={"com.pdd.account.tourist.api","com.pdd.commons","com.pdd.feign"},exclude = DataSourceAutoConfiguration.class)
@EnableEurekaClient
@EnableFeignClients
public class PddBlogAccountTouristApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(PddBlogAccountTouristApiApplication.class,args);
    }
}
