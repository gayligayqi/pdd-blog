package com.pdd.account.tourist.api.controller;

import com.alibaba.fastjson.JSONObject;
import com.pdd.account.tourist.api.feign.API_Blog_Account;
import com.pdd.commons.entity.dao.Blog_Account;
import com.pdd.commons.entity.dto.LoginForm;
import com.pdd.commons.entity.dto.RegisterForm;
import com.pdd.commons.response.ResponseEntity;
import com.pdd.commons.utils.PublicKey;
import com.pdd.commons.utils.WebAreaUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Map;

/**
 * @author:liyangpeng
 * @date:2019/6/21 16:08
 */
@RestController
@RequestMapping("/blog_account")
public class Blog_AccountController {

    @Autowired
    private API_Blog_Account api_blog_account;
    @Autowired
    private WebAreaUtil webAreaUtil;
    /**
     * 登录接口
     * @param loginForm
     * @return
     */
    @PostMapping("/login")
    public ResponseEntity login(@Valid @RequestBody LoginForm loginForm){
        ResponseEntity entity= api_blog_account.login(loginForm);
        if(entity.isSuccess()){
            JSONObject loginResult=(JSONObject)entity.getData();
            Cookie cookie=new Cookie(PublicKey.SESSION_KEY,loginResult.getString(PublicKey.SESSION_KEY));
            cookie.setPath("/");
            HttpServletResponse response=webAreaUtil.getResponse();
            response.addCookie(cookie);
        }
        return entity;
    }

    /**
     * 用户主页查询用户信息接口
     * @param author_id
     * @return
     */
    @GetMapping("/getUserIndexData")
    public ResponseEntity getUserIndexData(Integer author_id){
        return api_blog_account.getUserIndexData(author_id);
    }

    /**
     * 用户注册
     * @param registerForm
     * @return
     */
    @PostMapping("/register")
    public ResponseEntity register(@Valid @RequestBody RegisterForm registerForm){
        return api_blog_account.register(registerForm);
    }

    /**
     * 发送邮箱验证码
     * @return
     */
    @PostMapping("/sendMailValidCode")
    public ResponseEntity sendMailValidCode(@RequestBody Map<String,String> param){
        return api_blog_account.sendMailValidCode(param);
    }
}
