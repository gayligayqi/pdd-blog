package com.pdd.account.tourist.api.fallback;

import com.pdd.account.tourist.api.feign.API_Blog_Account;
import com.pdd.account.tourist.enums.WebResultEmun;
import com.pdd.account.tourist.response.ResponseResult;
import com.pdd.commons.entity.dto.LoginForm;
import com.pdd.commons.entity.dto.RegisterForm;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import java.util.Map;

/**
 * @author:liyangpeng
 * @date:2019/7/11 16:53
 */
@Component
public class Blog_AccountFallback implements API_Blog_Account {

    @Override
    public ResponseEntity login(@Valid LoginForm loginForm) {
        return ResponseResult.Return(WebResultEmun.SERVER_INVOKE_LOGIN_ERROR);
    }

    @Override
    public ResponseEntity getUserIndexData(Integer author_id) {
        return ResponseResult.Return(WebResultEmun.SERVER_INVOKE_USER_INDEX_DATA_ERROR);
    }

    @Override
    public ResponseEntity register(@Valid RegisterForm registerForm) {
        return ResponseResult.Return(WebResultEmun.SERVER_INVOKE_REGISTER_ERROR);
    }

    @Override
    public ResponseEntity sendMailValidCode(Map<String, String> param) {
        return ResponseResult.Return(WebResultEmun.SERVER_INVOKE_SEND_MAIN_VALID_CODE_ERROR);
    }
}
