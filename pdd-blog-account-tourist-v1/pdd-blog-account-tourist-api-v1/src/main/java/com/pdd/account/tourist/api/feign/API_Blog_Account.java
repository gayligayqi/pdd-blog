package com.pdd.account.tourist.api.feign;

import com.pdd.account.tourist.api.fallback.Blog_AccountFallback;
import com.pdd.commons.entity.dto.LoginForm;
import com.pdd.commons.entity.dto.RegisterForm;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import javax.validation.Valid;
import java.util.Map;

/**
 * @author:liyangpeng
 * @date:2019/6/21 16:08
 */
@FeignClient(value = "pdd-blog-account-tourist-server-v1", path = "blog_account", fallback = Blog_AccountFallback.class)
public interface API_Blog_Account {

    /**
     *  登录接口
     * @param loginForm
     * @return
     */
    @PostMapping("/login")
    ResponseEntity login(@Valid @RequestBody LoginForm loginForm);

    /**
     * 用户主页查询用户信息接口
     * @param author_id
     * @return
     */
    @GetMapping("/getUserIndexData")
    ResponseEntity getUserIndexData(@RequestParam("author_id") Integer author_id);

    /**
     * 用户注册
     * @param registerForm
     * @return
     */
    @PostMapping("/register")
    ResponseEntity register(@Valid @RequestBody RegisterForm registerForm);

    /**
     * 发送邮箱验证码
     * @return
     */
    @PostMapping("/sendMailValidCode")
    ResponseEntity sendMailValidCode(@RequestBody Map<String, String> param);

}
