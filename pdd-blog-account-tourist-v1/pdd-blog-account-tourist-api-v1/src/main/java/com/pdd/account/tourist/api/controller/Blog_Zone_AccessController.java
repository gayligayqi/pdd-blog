package com.pdd.account.tourist.api.controller;

import com.pdd.account.tourist.api.feign.API_Blog_Zone_Access;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author:liyangpeng
 * @date:2019/7/16 9:43
 */
@RestController
@RequestMapping("/blog_zone_access")
public class Blog_Zone_AccessController {

    @Autowired
    private API_Blog_Zone_Access api_blog_zone_access;

    /**
     * 查询空间访问记录接口
     * @param zoneId
     * @return
     */
    @GetMapping("/getZoneAccess")
    public ResponseEntity getZoneAccess(Integer zoneId){
        return api_blog_zone_access.getZoneAccess(zoneId);
    };
}
