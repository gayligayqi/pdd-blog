package com.pdd.account.tourist.api.controller;

import com.pdd.account.tourist.api.feign.API_Blog_Account_AchieveMent_Log;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author:liyangpeng
 * @date:2019/7/16 14:35
 */
@RestController
@RequestMapping("/achieveMent")
public class Blog_Account_AchieveMent_LogController {

    @Autowired
    private API_Blog_Account_AchieveMent_Log api_blog_account_achieveMent_log;

    /**
     * 查询历史成就接口
     * @param author_id
     * @return
     */
    @GetMapping("/getAccountAchiveMentLog")
    public ResponseEntity getAccountAchiveMentLog(Integer author_id) {
        return api_blog_account_achieveMent_log.getAccountAchiveMentLog(author_id);
    }
}