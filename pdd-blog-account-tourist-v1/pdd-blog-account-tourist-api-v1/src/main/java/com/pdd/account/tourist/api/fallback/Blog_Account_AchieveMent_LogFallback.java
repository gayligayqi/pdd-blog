package com.pdd.account.tourist.api.fallback;

import com.pdd.account.tourist.api.feign.API_Blog_Account_AchieveMent_Log;
import com.pdd.account.tourist.enums.WebResultEmun;
import com.pdd.account.tourist.response.ResponseResult;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.stereotype.Component;

/**
 * @author:liyangpeng
 * @date:2019/7/16 14:21
 */
@Component
public class Blog_Account_AchieveMent_LogFallback implements API_Blog_Account_AchieveMent_Log {

    @Override
    public ResponseEntity getAccountAchiveMentLog(Integer author_id) {
        return ResponseResult.Return(WebResultEmun.SERVER_INVOKE_ACCOUNT_ACHIEVE_MENT_LOG_ERROR);
    }
}
