package com.pdd.account.tourist.enums;

/**
 * @author:liyangpeng
 * @date:2019/7/11 16:26
 */
public enum WebResultEmun {
    SUCCESS("PBACT_0000","请求成功",true),
    ACCOUNT_LOGIN_SUCCESS("PBACT_L0000","登录成功",true),
    ACCOUNT_NOT_EXISTS_ERROR("PBACT_L0001","账号不存在",false),
    ACCOUNT_OR_PASSWORD_ERROR("PBACT_L0002","账号或密码不正确",false),
    REGISTER_ACCOUNT_EXISTS("PBACT_R0001","用户名已经存在",false),
    REGISTER_NICKANME_EXISTS("PBACT_R0002","昵称已经存在",false),
    REGISTER_EMAIL_EXISTS("PBACT_R0003","邮箱已经存在",false),
    REGISTER_CODE_ERROR("PBACT_R0004","验证码错误",false),
    REGISTER_ERROR("PBACT_R0004","注册失败",false),
    REGISTER_SUCCESS("PBACT_R0000","注册成功",true),
    EMAIL_SEND_ERROR("PBACT_E0002","邮件发送失败",false),
    EMAIL_SEND_SUCCESS("PBACT_E0000","邮件发送成功",true),
    SERVER_INVOKE_LOGIN_ERROR("PBACT_SIE0001","登录失败,请稍后再重新提交,错误码:PBACT_SIE0001",false),
    SERVER_INVOKE_USER_INDEX_DATA_ERROR("PBACT_SIE0002","获取用户信息失败,请稍后再试,错误码:PBACT_SIE0002",false),
    SERVER_INVOKE_REGISTER_ERROR("PBACT_SIE0003","注册失败,请稍后再重新提交,错误码:PBACT_SIE0003",false),
    SERVER_INVOKE_SEND_MAIN_VALID_CODE_ERROR("PBACT_SIE0004","邮箱验证码发送失败,错误码:PBACT_SIE0004",false),
    SERVER_INVOKE_ZONE_ACCESS_ERROR("PBACT_SIE0005","查询主页访问记录失败,错误码:PBACT_SIE0005",false),
    SERVER_INVOKE_ACCOUNT_ACHIEVE_MENT_LOG_ERROR("PBACT_SIE0006","查询历史成就失败,错误码:PBACT_SIE0006",false)
    ;

    private String code;
    private String msg;
    private boolean success;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    WebResultEmun(String code, String msg, boolean success) {
        this.code = code;
        this.msg = msg;
        this.success=success;
    }
}
