package com.pdd.account.tourist.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.pdd.commons.entity.dao.Blog_Zone_Access;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;

/**
 * @author:liyangpeng
 * @date:2019/7/16 9:48
 */
@Mapper
public interface Blog_Zone_AccessMapper extends BaseMapper<Blog_Zone_Access> {

    IPage<HashMap<String,Object>> getZoneAccess(IPage iPage, @Param("zoneId") Integer zoneId);
}
