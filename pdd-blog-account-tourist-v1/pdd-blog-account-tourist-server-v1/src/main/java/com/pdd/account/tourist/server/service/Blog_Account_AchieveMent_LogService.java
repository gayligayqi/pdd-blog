package com.pdd.account.tourist.server.service;

import com.pdd.commons.response.ResponseEntity;

/**
 * @author:liyangpeng
 * @date:2019/7/16 14:18
 */
public interface Blog_Account_AchieveMent_LogService {

    ResponseEntity getAccountAchiveMentLog(Integer author_id);
}
