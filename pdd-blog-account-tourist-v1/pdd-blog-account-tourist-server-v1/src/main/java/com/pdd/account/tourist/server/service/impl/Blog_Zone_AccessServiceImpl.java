package com.pdd.account.tourist.server.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pdd.account.tourist.response.ResponseResult;
import com.pdd.account.tourist.server.mapper.Blog_Zone_AccessMapper;
import com.pdd.account.tourist.server.service.Blog_Zone_AccessService;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * @author:liyangpeng
 * @date:2019/7/16 9:47
 */
@Service
public class Blog_Zone_AccessServiceImpl implements Blog_Zone_AccessService {

    @Autowired
    private Blog_Zone_AccessMapper blog_zone_accessMapper;

    @Override
    public ResponseEntity getZoneAccess(Integer zoneId) {
        IPage<HashMap<String,Object>> iPage=blog_zone_accessMapper.getZoneAccess(new Page<HashMap<String,Object>>(0,12),zoneId);
        return ResponseResult.Return(iPage);
    }
}
