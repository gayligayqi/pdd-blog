package com.pdd.account.tourist.server.controller;

import com.pdd.account.tourist.api.feign.API_Blog_Account;
import com.pdd.account.tourist.server.service.Blog_AccountService;
import com.pdd.commons.entity.dto.LoginForm;
import com.pdd.commons.entity.dto.RegisterForm;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Map;

/**
 * @author:liyangpeng
 * @date:2019/6/21 16:08
 */
@RestController
@RequestMapping("/blog_account")
public class Blog_AccountController  implements API_Blog_Account{

    @Autowired
    private Blog_AccountService blog_accountService;

    /**
     * 登录接口
     * @param loginForm
     * @return
     */
    @Override
    public ResponseEntity login(@Valid @RequestBody LoginForm loginForm){
        return blog_accountService.login(loginForm);
    }

    /**
     * 用户主页查询用户信息接口
     * @param author_id
     * @return
     */
    @Override
    public ResponseEntity getUserIndexData(Integer author_id){
        return blog_accountService.getUserIndexData(author_id);
    }

    /**
     * 用户注册
     * @param registerForm
     * @return
     */
    @Override
    public ResponseEntity register(@Valid @RequestBody RegisterForm registerForm){
        return blog_accountService.register(registerForm);
    }

    /**
     * 发送邮箱验证码
     * @return
     */
    @Override
    public ResponseEntity sendMailValidCode(@RequestBody Map<String,String> param){
        return blog_accountService.sendMailValidCode(param);
    }
}
