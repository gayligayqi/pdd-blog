package com.pdd.account.tourist.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pdd.commons.entity.dao.Blog_Account;
import com.pdd.commons.entity.vo.UserIndex_Vo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author:liyangpeng
 * @date:2019/6/21 16:06
 */
@Mapper
public interface Blog_AccountMapper extends BaseMapper<Blog_Account> {

    UserIndex_Vo getUserIndexData(@Param("author_id") Integer author_id, @Param("self_author_id") Integer self_author_id);

    Long validAccount(String account);

    Long validNiciName(String nickName);

    Long validEmail(String email);
}
