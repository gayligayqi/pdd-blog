package com.pdd.account.tourist.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author:liyangpeng
 * @date:2019/7/11 16:32
 */
@SpringBootApplication(scanBasePackages = {"com.pdd.account.tourist.server","com.pdd.commons"},excludeName ="com.pdd.account.tourist.api")
@EnableEurekaClient
public class PddBlogAccountTouristServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(PddBlogAccountTouristServerApplication.class,args);
    }
}
