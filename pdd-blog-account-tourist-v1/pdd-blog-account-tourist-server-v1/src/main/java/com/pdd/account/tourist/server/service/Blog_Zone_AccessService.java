package com.pdd.account.tourist.server.service;

import com.pdd.commons.response.ResponseEntity;

/**
 * @author:liyangpeng
 * @date:2019/7/16 9:46
 */
public interface Blog_Zone_AccessService {

    ResponseEntity getZoneAccess(Integer zoneId);
}
