package com.pdd.account.tourist.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pdd.account.tourist.response.ResponseResult;
import com.pdd.account.tourist.server.mapper.Blog_Account_AchieveMent_LogMapper;
import com.pdd.account.tourist.server.service.Blog_Account_AchieveMent_LogService;
import com.pdd.commons.entity.dao.Blog_Account_AchieveMent_Log;
import com.pdd.commons.response.ResponseEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author:liyangpeng
 * @date:2019/7/16 14:19
 */
@Service
@Slf4j
public class Blog_Account_AchieveMent_LogServiceImpl implements Blog_Account_AchieveMent_LogService {

    @Autowired
    private Blog_Account_AchieveMent_LogMapper blog_account_achieveMent_logMapper;

    @Override
    public ResponseEntity getAccountAchiveMentLog(Integer author_id) {
        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.eq("author_id",author_id);
        queryWrapper.eq("isdel",0);
        IPage<Blog_Account_AchieveMent_Log> iPage=blog_account_achieveMent_logMapper.selectPage(new Page<>(0,6),queryWrapper);
        return ResponseResult.Return(iPage);
    }
}
