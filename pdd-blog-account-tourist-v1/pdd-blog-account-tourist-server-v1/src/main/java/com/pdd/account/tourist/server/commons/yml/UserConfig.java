package com.pdd.account.tourist.server.commons.yml;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author:liyangpeng
 * @date:2019/7/3 17:28
 */
@Data
@Component
@ConfigurationProperties(prefix = "user")
public class UserConfig {
    /**
     * 头像
     */
    private String avatar;
    /**
     * 背景图
     */
    private String backImg;
}
