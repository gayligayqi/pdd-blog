package com.pdd.account.tourist.server.controller;

import com.pdd.account.tourist.api.feign.API_Blog_Zone_Access;
import com.pdd.account.tourist.server.service.Blog_Zone_AccessService;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author:liyangpeng
 * @date:2019/7/16 9:43
 */
@RestController
@RequestMapping("/blog_zone_access")
public class Blog_Zone_AccessController implements API_Blog_Zone_Access {

    @Autowired
    private Blog_Zone_AccessService blog_zone_accessService;

    /**
     * 查询用户主页访问记录
     * @param zoneId
     * @return
     */
    @Override
    public ResponseEntity getZoneAccess(Integer zoneId) {
        return blog_zone_accessService.getZoneAccess(zoneId);
    }
}
