package com.pdd.account.tourist.server.service;

import com.pdd.commons.entity.dto.LoginForm;
import com.pdd.commons.entity.dto.RegisterForm;
import com.pdd.commons.response.ResponseEntity;

import java.util.Map;

/**
 * @author:liyangpeng
 * @date:2019/6/21 16:07
 */
public interface Blog_AccountService {

    ResponseEntity login(LoginForm loginForm);

    ResponseEntity getUserIndexData(Integer author_id);

    ResponseEntity register(RegisterForm registerForm);

    ResponseEntity sendMailValidCode(Map<String, String> param);
}
