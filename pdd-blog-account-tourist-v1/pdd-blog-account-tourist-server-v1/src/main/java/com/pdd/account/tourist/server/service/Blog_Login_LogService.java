package com.pdd.account.tourist.server.service;

import com.pdd.commons.response.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

/**
 * @author:liyangpeng
 * @date:2019/6/27 20:01
 */
public interface Blog_Login_LogService {

    ResponseEntity addLog(Integer author_id, String platform, long login_time);
}
