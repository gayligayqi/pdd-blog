package com.pdd.account.tourist.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pdd.commons.entity.dao.Blog_Account_AchieveMent_Log;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author:liyangpeng
 * @date:2019/7/16 14:18
 */
@Mapper
public interface Blog_Account_AchieveMent_LogMapper extends BaseMapper<Blog_Account_AchieveMent_Log> {

}
