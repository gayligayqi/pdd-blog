package com.pdd.account.tourist.server.commons.util;

import java.util.Random;

/**
 * @author:liyangpeng
 * @date:2019/7/2 15:11
 */
public class RandomValidateCodeUtil {
    /**
     * 随机产生数字与字母组合的字符串
     */
    private static final String randString = "0123456789abcdefghijkmnpqrstuvwxyz";
    /**
     * 获取随机的字符
     */
    private static String getRandomString(int num) {
        return String.valueOf(randString.charAt(num));
    }

    /**
     * 生成验证码
     * @return
     */
    public static String getRandomCode(Integer length){
        Random random=new Random();
        StringBuffer stringBuffer=new StringBuffer();
        for (int i=0;i < length; i++){
            stringBuffer.append(getRandomString(random.nextInt(randString.length())));
        }
        return stringBuffer.toString();
    }
}
