package com.pdd.account.tourist.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pdd.commons.entity.dao.Blog_Login_Log;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author:liyangpeng
 * @date:2019/6/27 20:01
 */
@Mapper
public interface Blog_Login_LogMapper extends BaseMapper<Blog_Login_Log> {

}
