package com.pdd.account.tourist.server.service.impl;

import com.alibaba.fastjson.JSON;
import com.pdd.account.tourist.response.ResponseResult;
import com.pdd.account.tourist.server.mapper.Blog_Login_LogMapper;
import com.pdd.account.tourist.server.service.Blog_Login_LogService;
import com.pdd.commons.entity.dao.Blog_Login_Log;
import com.pdd.commons.response.ResponseEntity;
import com.pdd.commons.utils.UserAgentUtils;
import com.pdd.commons.utils.WebAreaUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * @author:liyangpeng
 * @date:2019/6/27 20:02
 */
@Service
@Slf4j
public class Blog_Login_LogServiceImpl implements Blog_Login_LogService {

    @Autowired
    private Blog_Login_LogMapper blog_login_logMapper;
    @Autowired
    private WebAreaUtil webAreaUtil;

    @Override
    public ResponseEntity addLog(Integer author_id, String platform, long login_time) {
        HttpServletRequest request=webAreaUtil.getRequest();
        Blog_Login_Log blog_login_log=new Blog_Login_Log();
        blog_login_log.setAuthor_id(author_id);
        blog_login_log.setPlatform(platform);
        blog_login_log.setIp(UserAgentUtils.getIp(request));
        blog_login_log.setOsname(UserAgentUtils.getOsName(request));
        blog_login_log.setBrowser(UserAgentUtils.getBrowserName(request));
        blog_login_log.setVersion(UserAgentUtils.getBrowserVersion(request));
        blog_login_log.setAddtime(new Date(login_time));
        int result=blog_login_logMapper.insert(blog_login_log);
        if(result==0){
            log.info("【pdd博客】记录登录日志失败:{}", JSON.toJSONString(blog_login_log));
            return ResponseResult.RETURN_ERROR("记录登录日志失败");
        }
        return ResponseResult.RETURN_SUCCESS("记录登录日志成功");
    }
}
