package com.pdd.account.tourist.server.controller;

import com.pdd.account.tourist.api.feign.API_Blog_Account_AchieveMent_Log;
import com.pdd.account.tourist.server.service.Blog_Account_AchieveMent_LogService;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author:liyangpeng
 * @date:2019/7/16 14:20
 */
@RestController
@RequestMapping("/achieveMent")
public class Blog_Account_AchieveMent_LogController implements API_Blog_Account_AchieveMent_Log {

    @Autowired
    private Blog_Account_AchieveMent_LogService blog_account_achieveMent_logService;

    /**
     * 查询历史成就接口
     * @param author_id
     * @return
     */
    @Override
    public ResponseEntity getAccountAchiveMentLog(Integer author_id) {
        return blog_account_achieveMent_logService.getAccountAchiveMentLog(author_id);
    }
}
