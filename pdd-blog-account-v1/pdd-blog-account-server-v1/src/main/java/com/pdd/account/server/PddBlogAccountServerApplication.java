package com.pdd.account.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author:liyangpeng
 * @date:2019/7/11 16:32
 */
@SpringBootApplication(scanBasePackages = {"com.pdd.account.server","com.pdd.commons"},excludeName ="com.pdd.account.api")
@EnableEurekaClient
public class PddBlogAccountServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(PddBlogAccountServerApplication.class,args);
    }
}
