package com.pdd.account.server.service.impl;

import com.alibaba.fastjson.JSON;
import com.pdd.account.enums.WebResultEmun;
import com.pdd.account.response.ResponseResult;
import com.pdd.account.server.mapper.Blog_AccountMapper;
import com.pdd.account.server.service.Blog_AccountService;
import com.pdd.commons.entity.dao.Blog_Account;
import com.pdd.commons.entity.dto.ModifyUserInfoForm;
import com.pdd.commons.entity.vo.UserInfo_Vo;
import com.pdd.commons.response.ResponseEntity;
import com.pdd.commons.utils.AccountUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author:liyangpeng
 * @date:2019/6/21 16:07
 */
@Service
@Slf4j
public class Blog_AccountServiceImpl implements Blog_AccountService {

    @Autowired
    private Blog_AccountMapper blog_accountMapper;
    @Autowired
    private AccountUtil accountUtil;

    @Override
    public ResponseEntity loginOut() {
        return accountUtil.loginOut();
    }

    @Override
    public Blog_Account getAuthorById(Integer author_id) {
        return blog_accountMapper.selectById(author_id);
    }

    @Override
    public ResponseEntity getUserInfo() {
        Blog_Account blog_account=blog_accountMapper.selectById(accountUtil.getLoginInfo().getId());
        UserInfo_Vo userInfo_vo=new UserInfo_Vo();
        BeanUtils.copyProperties(blog_account,userInfo_vo);
        if(blog_account==null){
            return ResponseResult.Return(WebResultEmun.NOT_FOUND_USER_ERROR);
        }
        return ResponseResult.Return(userInfo_vo);
    }

    @Override
    public ResponseEntity modifyUserInfo(ModifyUserInfoForm modifyUserInfoForm) {
        Blog_Account blog_account=new Blog_Account();
        BeanUtils.copyProperties(modifyUserInfoForm,blog_account);
        blog_account.setId(accountUtil.getLoginInfo().getId());
        boolean modifyResult=blog_account.updateById();
        if(modifyResult){
            log.info("【pdd博客-修改个人信息】修改个人信息成功:{}", JSON.toJSONString(blog_account));
            return ResponseResult.Return(WebResultEmun.MODIFY_USER_INFO_SUCCESS);
        }
        log.debug("【pdd博客-修改个人信息】修改个人信息失败:{}", JSON.toJSONString(blog_account));
        return ResponseResult.Return(WebResultEmun.MODIFY_USER_INFO_ERROR);
    }
}
