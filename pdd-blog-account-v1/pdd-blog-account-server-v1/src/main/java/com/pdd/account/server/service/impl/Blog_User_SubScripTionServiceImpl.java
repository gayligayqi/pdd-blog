package com.pdd.account.server.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.pdd.account.enums.WebResultEmun;
import com.pdd.account.response.ResponseResult;
import com.pdd.account.server.mapper.Blog_User_SubScripTionMapper;
import com.pdd.account.server.service.Blog_User_SubScripTionService;
import com.pdd.commons.entity.dao.Blog_User_SubScripTion;
import com.pdd.commons.entity.vo.AccountInfo_Vo;
import com.pdd.commons.response.ResponseEntity;
import com.pdd.commons.utils.AccountUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author:liyangpeng
 * @date:2019/8/13 9:59
 */
@Service
public class Blog_User_SubScripTionServiceImpl implements Blog_User_SubScripTionService {

    @Autowired
    private Blog_User_SubScripTionMapper blog_user_subScripTionMapper;
    @Autowired
    private AccountUtil accountUtil;

    @Override
    public ResponseEntity add(JSONObject jsonObject) {
        if(jsonObject==null||jsonObject.getInteger("author_id")==null||jsonObject.getInteger("stat")==null){
            return ResponseResult.RETURN_ERROR("参数错误");
        }
        AccountInfo_Vo accountInfo_vo=accountUtil.getLoginInfo();
        Blog_User_SubScripTion blog_user_subScripTion=blog_user_subScripTionMapper.selectOne(Wrappers.<Blog_User_SubScripTion>query()
                .eq("author_id",accountInfo_vo.getId())
                .eq("care_author_id",jsonObject.getInteger("author_id")));
        boolean result=false;
        if(blog_user_subScripTion!=null){
            blog_user_subScripTion.setIssubc(jsonObject.getInteger("stat"));
            result=blog_user_subScripTion.updateById();
        }else{
            blog_user_subScripTion=new Blog_User_SubScripTion();
            blog_user_subScripTion.setAuthor_id(accountInfo_vo.getId());
            blog_user_subScripTion.setCare_author_id(jsonObject.getInteger("author_id"));
            blog_user_subScripTion.setIssubc(jsonObject.getInteger("stat"));
            result=blog_user_subScripTion.insert();
        }
        if(result){
            return ResponseResult.Return(WebResultEmun.SUBSCRIPTION_ADD_SUCCESS);
        }else{
            return ResponseResult.Return(WebResultEmun.SUBSCRIPTION_ADD_ERROR);
        }
    }
}
