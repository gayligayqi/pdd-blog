package com.pdd.account.server.controller;

import com.pdd.account.api.feign.API_Blog_Account;
import com.pdd.account.server.service.Blog_AccountService;
import com.pdd.commons.entity.dao.Blog_Account;
import com.pdd.commons.entity.dto.ModifyUserInfoForm;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author:liyangpeng
 * @date:2019/6/21 16:08
 */
@RestController
@RequestMapping("/blog_account")
public class Blog_AccountController implements API_Blog_Account {

    @Autowired
    private Blog_AccountService blog_accountService;

    /**
     * 退出登录
     * @return
     */
    @Override
    public ResponseEntity loginOut(){
        return blog_accountService.loginOut();
    }

    /**
     * 根据id查询用户信息
     * @param author_id
     * @return
     */
    @Override
    public Blog_Account getAuthorById(Integer author_id) {
        return blog_accountService.getAuthorById(author_id);
    }

    /**
     * 登录后查询用户信息
     * @return
     */
    @Override
    public ResponseEntity getUserInfo() {
        return blog_accountService.getUserInfo();
    }

    /**
     * 修改个人信息
     * @param modifyUserInfoForm
     * @return
     */
    @Override
    public ResponseEntity modifyUserInfo(ModifyUserInfoForm modifyUserInfoForm) {
        return blog_accountService.modifyUserInfo(modifyUserInfoForm);
    }
}
