package com.pdd.account.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pdd.commons.entity.dao.Blog_Account;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author:liyangpeng
 * @date:2019/6/21 16:06
 */
@Mapper
public interface Blog_AccountMapper extends BaseMapper<Blog_Account> {

}
