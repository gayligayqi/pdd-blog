package com.pdd.account.server.service;

import com.pdd.commons.entity.dao.Blog_Account;
import com.pdd.commons.entity.dto.ModifyUserInfoForm;
import com.pdd.commons.response.ResponseEntity;

import javax.xml.ws.Response;

/**
 * @author:liyangpeng
 * @date:2019/6/21 16:07
 */
public interface Blog_AccountService {

    ResponseEntity loginOut();

    Blog_Account getAuthorById(Integer author_id);

    ResponseEntity getUserInfo();

    ResponseEntity modifyUserInfo(ModifyUserInfoForm modifyUserInfoForm);
}
