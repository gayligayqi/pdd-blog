package com.pdd.account.server.service;

import com.alibaba.fastjson.JSONObject;
import com.pdd.commons.response.ResponseEntity;

/**
 * @author:liyangpeng
 * @date:2019/8/13 9:58
 */
public interface Blog_User_SubScripTionService {

    ResponseEntity add(JSONObject jsonObject);
}