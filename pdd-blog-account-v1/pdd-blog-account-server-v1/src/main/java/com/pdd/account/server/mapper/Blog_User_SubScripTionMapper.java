package com.pdd.account.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pdd.commons.entity.dao.Blog_User_SubScripTion;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author:liyangpeng
 * @date:2019/8/13 9:58
 */
@Mapper
public interface Blog_User_SubScripTionMapper extends BaseMapper<Blog_User_SubScripTion> {

}
