package com.pdd.account.enums;

/**
 * @author:liyangpeng
 * @date:2019/6/19 13:54
 */
public enum WebResultEmun {
    SUCCESS("PBAC_0000","请求成功",true),
    NOT_FOUND_USER_ERROR("PBAC_0001","未找到用户信息",false),
    MODIFY_USER_INFO_SUCCESS("PBAC_0002","修改用户信息成功",true),
    MODIFY_USER_INFO_ERROR("PBAC_0003","修改用户信息失败",false),
    SUBSCRIPTION_ADD_SUCCESS("PBAC_SAE0001","订阅成功",true),
    SUBSCRIPTION_ADD_ERROR("PBAC_SAE0002","订阅失败",false),
    SERVER_INVOKE_LOGIN_OUT_ERROR("PBAC_SIE0001","退出登录失败,错误码:PBA_SIE0001",false),
    SERVER_INVOKE_USER_INFO_ERROR("PBAC_SIE0002","查询用户信息失败,错误码:PBA_SIE0003",false),
    SERVER_INVOKE_MODIFY_USER_INFO_ERROR("PBAC_SIE0003","修改用户信息失败,错误码:PBAC_SIE0003",false),
    SERVER_INVOKE_SUBSCRIPTION_ADD_ERROR("PBAC_SIE0004","订阅失败,错误码:PBAC_SIE0004",false)
    ;

    private String code;
    private String msg;
    private boolean success;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    WebResultEmun(String code, String msg, boolean success) {
        this.code = code;
        this.msg = msg;
        this.success=success;
    }
}
