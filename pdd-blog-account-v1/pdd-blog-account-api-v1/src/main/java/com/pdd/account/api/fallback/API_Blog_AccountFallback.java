package com.pdd.account.api.fallback;

import com.pdd.account.api.feign.API_Blog_Account;
import com.pdd.account.enums.WebResultEmun;
import com.pdd.account.response.ResponseResult;
import com.pdd.commons.entity.dao.Blog_Account;
import com.pdd.commons.entity.dto.ModifyUserInfoForm;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.stereotype.Component;

/**
 * @author:liyangpeng
 * @date:2019/7/16 15:49
 */
@Component
public class API_Blog_AccountFallback implements API_Blog_Account {

    @Override
    public ResponseEntity loginOut() {
        return ResponseResult.Return(WebResultEmun.SERVER_INVOKE_LOGIN_OUT_ERROR);
    }

    @Override
    public Blog_Account getAuthorById(Integer author_id) {
        return null;
    }

    @Override
    public ResponseEntity getUserInfo() {
        return ResponseResult.Return(WebResultEmun.SERVER_INVOKE_USER_INFO_ERROR);
    }

    @Override
    public ResponseEntity modifyUserInfo(ModifyUserInfoForm modifyUserInfoForm) {
        return ResponseResult.Return(WebResultEmun.SERVER_INVOKE_MODIFY_USER_INFO_ERROR);
    }
}
