package com.pdd.account.api.feign;

import com.pdd.account.api.fallback.API_Blog_AccountFallback;
import com.pdd.commons.entity.dao.Blog_Account;
import com.pdd.commons.entity.dto.ModifyUserInfoForm;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author:liyangpeng
 * @date:2019/7/16 15:49
 */
@FeignClient(value = "pdd-blog-account-server-v1",path = "blog_account",fallback= API_Blog_AccountFallback.class)
public interface API_Blog_Account  {

    /**
     * 退出登录
     * @return
     */
    @PostMapping("/loginOut")
    ResponseEntity loginOut();

    /**
     * 跟id查询账号信息
     * @param author_id
     * @return
     */
    @GetMapping("/getAuthorById")
    Blog_Account getAuthorById(@RequestParam("author_id") Integer author_id);

    /**
     * 登录后查询自己的用户信息
     * @return
     */
    @GetMapping("/getUserInfo")
    ResponseEntity getUserInfo();

    /**
     * 修改用户信息
     * @param modifyUserInfoForm
     * @return
     */
    @PostMapping("/modifyUserInfo")
    ResponseEntity modifyUserInfo(@RequestBody ModifyUserInfoForm modifyUserInfoForm);
}
