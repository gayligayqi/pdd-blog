package com.pdd.account.api.fallback;

import com.alibaba.fastjson.JSONObject;
import com.pdd.account.api.feign.API_Blog_User_SubScripTion;
import com.pdd.account.enums.WebResultEmun;
import com.pdd.account.response.ResponseResult;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.stereotype.Component;

/**
 * @author:liyangpeng
 * @date:2019/8/13 10:15
 */
@Component
public class API_Blog_User_SubScripTionFallBack implements API_Blog_User_SubScripTion {

    @Override
    public ResponseEntity add(JSONObject jsonObject) {
        return ResponseResult.Return(WebResultEmun.SERVER_INVOKE_SUBSCRIPTION_ADD_ERROR);
    }
}
