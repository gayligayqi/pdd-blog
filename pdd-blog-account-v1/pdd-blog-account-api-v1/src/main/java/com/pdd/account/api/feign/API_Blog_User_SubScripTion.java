package com.pdd.account.api.feign;

import com.alibaba.fastjson.JSONObject;
import com.pdd.account.api.fallback.API_Blog_User_SubScripTionFallBack;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author:liyangpeng
 * @date:2019/8/13 10:14
 */
@FeignClient(value = "pdd-blog-account-server-v1",path = "subscrip",fallback= API_Blog_User_SubScripTionFallBack.class)
public interface API_Blog_User_SubScripTion {
    /**
     * 订阅/取消订阅
     * @param jsonObject
     * @return
     */
    @PostMapping("/add")
    ResponseEntity add(@RequestBody JSONObject jsonObject);

}
