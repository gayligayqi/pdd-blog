package com.pdd.account.api.controller;

import com.pdd.account.api.feign.API_Blog_Account;
import com.pdd.commons.entity.dao.Blog_Account;
import com.pdd.commons.entity.dto.ModifyUserInfoForm;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author:liyangpeng
 * @date:2019/7/16 15:50
 */
@RestController
@RequestMapping("/blog_account")
public class Blog_AccountController {

    @Autowired
    private API_Blog_Account api_blog_account;

    /**
     * 退出登录
     * @return
     */
    @PostMapping("/loginOut")
    public ResponseEntity loginOut(){
        return api_blog_account.loginOut();
    }
    /**
     * 根据id查询用户信息
     * @param author_id
     * @return
     */
    @GetMapping("/getAuthorById")
    public Blog_Account getAuthorById(Integer author_id) {
        return api_blog_account.getAuthorById(author_id);
    }

    /**
     * 登录用户查询自己的信息
     * @return
     */
    @GetMapping("/getUserInfo")
    public ResponseEntity getUserInfo() {
        return api_blog_account.getUserInfo();
    }

    /**
     * 修改用户信息
     * @param modifyUserInfoForm
     * @return
     */
    @PostMapping("/modifyUserInfo")
    public ResponseEntity modifyUserInfo(@RequestBody @Valid  ModifyUserInfoForm modifyUserInfoForm){
        return api_blog_account.modifyUserInfo(modifyUserInfoForm);
    };
}
