package com.pdd.account.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author:liyangpeng
 * @date:2019/7/16 15:46
 */
@SpringBootApplication(scanBasePackages ={"com.pdd.account.api","com.pdd.commons","com.pdd.feign"},exclude = DataSourceAutoConfiguration.class)
@EnableEurekaClient
@EnableFeignClients
public class PddBlogAccountApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(PddBlogAccountApiApplication.class,args);
    }
}
