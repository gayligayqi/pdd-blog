package com.pdd.account.api.controller;

import com.alibaba.fastjson.JSONObject;
import com.pdd.account.api.feign.API_Blog_User_SubScripTion;
import com.pdd.commons.response.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author:liyangpeng
 * @date:2019/8/13 10:14
 */
@RestController
@RequestMapping("/subscrip")
public class Blog_User_SubScripTionController {

    @Autowired
    private API_Blog_User_SubScripTion api_blog_user_subScripTion;

    /**
     * 订阅/取消订阅
     * @param jsonObject
     * @return
     */
    @PostMapping("/add")
    public ResponseEntity add(@RequestBody  JSONObject jsonObject){
        return api_blog_user_subScripTion.add(jsonObject);
    };
}
